<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/sentado.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/cafe.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/calle.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/IMG-2529.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/IMG-2530.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/1_95d68-bg.JPG'
			]);
		DB::table('sliders')->insert([
				'sliders_ruta_imagen'       => 'imagenes/sliders/2_IMG-2534.JPG'
			]);
	}
}
