<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('videos')->insert([
				'nombre'     => 'Lo que aprenderás.',
				'descripcion'    => 'Lo que aprenderás.',
				'categoria_id'  => 1,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Qué expectativas tener.',
				'descripcion'    => 'Qué expectativas tener.',
				'categoria_id'  => 1,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Acciones necesarias.',
				'descripcion'    => 'Acciones necesarias.',
				'categoria_id'  => 1,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Plataformas necesarias y el cómo darle uso.',
				'descripcion'    => 'Plataformas necesarias y el cómo darle uso.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'MT4 Teléfono',
				'descripcion'    => 'MT4 Teléfono',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Broker',
				'descripcion'    => 'Broker',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Iniciando cuenta con tu broker.',
				'descripcion'    => 'Iniciando cuenta con tu broker.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Uso dentro de la plataforma broker.',
				'descripcion'    => 'Uso dentro de la plataforma broker.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 5,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Creando cuenta real/demo.',
				'descripcion'    => 'Creando cuenta real/demo.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 6,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Vinculando cuenta broker con MT4 teléfono.',
				'descripcion'    => 'Vinculando cuenta broker con MT4 teléfono.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 7,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Depositar y retirar.',
				'descripcion'    => 'Depositar y retirar.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 8,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Tradingview.',
				'descripcion'    => 'Tradingview.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 9,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Creando cuenta en Tradingview.',
				'descripcion'    => 'Creando cuenta en Tradingview.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 10,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Conocimiento general de Tradingview.',
				'descripcion'    => 'Conocimiento general de Tradingview.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 11,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Utilizar herramientas/favoritos en Tradingview.',
				'descripcion'    => 'Utilizar herramientas/favoritos en Tradingview.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 12,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'MT4.',
				'descripcion'    => 'MT4',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 13,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Descargar MT4 en Mac.',
				'descripcion'    => 'Descargar MT4 en Mac.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 14,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Vincular MT4 con broker.',
				'descripcion'    => 'Vincular MT4 con broker.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 15,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Conocimiento de MT4 general.',
				'descripcion'    => 'Conocimiento de MT4 general.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 16,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Crear templates en MT4.',
				'descripcion'    => 'Crear templates en MT4.',
				'categoria_id'  => 2,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 17,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Resistencia.',
				'descripcion'    => 'Resistencia.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Soporte.',
				'descripcion'    => 'Soporte.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Trend.',
				'descripcion'    => 'Trend.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Consolidación.',
				'descripcion'    => 'Consolidación.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Vela japonesa.',
				'descripcion'    => 'Vela japonesa.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 5,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Wick.',
				'descripcion'    => 'Wick.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 6,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Spread.',
				'descripcion'    => 'Spread.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 7,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Commission/broker.',
				'descripcion'    => 'Commission/broker.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 8,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Swap/broker.',
				'descripcion'    => 'Swap/broker.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 9,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Slippage.',
				'descripcion'    => 'Slippage.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 10,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Pending orders.',
				'descripcion'    => 'Pending orders.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 11,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Buy stop.',
				'descripcion'    => 'Buy stop.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 12,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Sell stop.',
				'descripcion'    => 'Sell stop.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 13,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Buy limit.',
				'descripcion'    => 'Buy limit.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 14,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Sell limit.',
				'descripcion'    => 'Sell limit.',
				'categoria_id'  => 3,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 15,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Soporte.',
				'descripcion'    => 'Soporte.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Resistencia.',
				'descripcion'    => 'Resistencia.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Tendencia.',
				'descripcion'    => 'Tendencia.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Tendencia Alcista.',
				'descripcion'    => 'Tendencia Alcista.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Tendencia Bajista.',
				'descripcion'    => 'Tendencia Bajista.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 5,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Consolidación.',
				'descripcion'    => 'Consolidación.',
				'categoria_id'  => 4,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 6,
			]);

		DB::table('videos')->insert([
				'nombre'     => 'Estructura.',
				'descripcion'    => 'Estructura.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 7,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Estructura Alcista.',
				'descripcion'    => 'Estructura Alcista.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 8,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Estructura Bajista.',
				'descripcion'    => 'Estructura Bajista.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 9,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Quiebre de Estructura “BOS”.',
				'descripcion'    => 'Quiebre de Estructura “BOS”.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 10,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Quiebre Bajista.',
				'descripcion'    => 'Quiebre Bajista.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 11,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Quiebre Alcista.',
				'descripcion'    => 'Quiebre Alcista.',
				'categoria_id'  => 5,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 12,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Acción de Precio.',
				'descripcion'    => 'Acción de Precio.',
				'categoria_id'  => 6,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Variaciones alcistas.',
				'descripcion'    => 'Variaciones alcistas.',
				'categoria_id'  => 6,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Variaciones bajistas.',
				'descripcion'    => 'Variaciones bajistas.',
				'categoria_id'  => 6,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'PDF.',
				'descripcion'    => 'PDF.',
				'categoria_id'  => 6,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Beneficio de temporalidades.',
				'descripcion'    => 'Beneficio de temporalidades.',
				'categoria_id'  => 7,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Correlación.',
				'descripcion'    => 'Correlación.',
				'categoria_id'  => 7,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'PDF.',
				'descripcion'    => 'PDF.',
				'categoria_id'  => 7,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Accion de Fib “impulsos”.',
				'descripcion'    => 'Accion de Fib “impulsos”.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'PDF Fibs.',
				'descripcion'    => 'PDF Fibs.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 5,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Temporalidades de Fib basado a Estructura.',
				'descripcion'    => 'Temporalidades de Fib basado a Estructura.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 6,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Colocación de Fib.',
				'descripcion'    => 'Colocación de Fib.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 7,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Fib estructura alcista.',
				'descripcion'    => 'Fib estructura alcista.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 8,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Fib estructura Bajista.',
				'descripcion'    => 'Fib estructura Bajista.',
				'categoria_id'  => 8,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 9,
			]);
		DB::table('videos')->insert([
				'nombre'     => '¿Qué son y que utilizamos?',
				'descripcion'    => '¿Qué son y que utilizamos?',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 1,
			]);
		DB::table('videos')->insert([
				'nombre'     => '¿Como utilizar las herramientas necesarias?',
				'descripcion'    => '¿Como utilizar las herramientas necesarias?',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 2,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'PDF en que me baso para noticias.',
				'descripcion'    => 'PDF en que me baso para noticias.',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 3,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Correlaciones fundamentales con técnico.',
				'descripcion'    => 'Correlaciones fundamentales con técnico.',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 4,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Datos importantes “Slippage”.',
				'descripcion'    => 'Datos importantes “Slippage”.',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 5,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Noticias de importancia.',
				'descripcion'    => 'Noticias de importancia.',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 6,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Ejemplo de operación Fundamental y técnico.',
				'descripcion'    => 'Ejemplo de operación Fundamental y técnico.',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 7,
			]);
		DB::table('videos')->insert([
				'nombre'     => '¿Cómo minimizar pérdidas y maximizar ganancias en noticias?',
				'descripcion'    => '¿Cómo minimizar pérdidas y maximizar ganancias en noticias?',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 8,
			]);
		DB::table('videos')->insert([
				'nombre'     => '% a arriesgar con noticias y el por qué',
				'descripcion'    => '% a arriesgar con noticias y el por qué',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 9,
			]);
		DB::table('videos')->insert([
				'nombre'     => 'Momento para asegurar ganancias operando noticias',
				'descripcion'    => 'Momento para asegurar ganancias operando noticias',
				'categoria_id'  => 10,
				'iframe'       => '<iframe></iframe>',
				'plataforma'	=> 0,
				'posicion'		=> 10,
			]);

	}
}
