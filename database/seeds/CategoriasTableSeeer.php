<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
///////////////////////// Inicio Categorias Español////////////////////////////////
		DB::table('categorias')->insert([
				'nombre'     => 'Video Bienvenido a C4X.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Inicio.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Definiciones y explicaciones.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 1: Identificar patrones técnicos.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 2: Identificando Estructuras.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 3: Acción de precio.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 4: Correlación de temporalidades.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 5: Fibonacci basado con Estructura.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fase 6: Entradas live grabadas.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fundamentales.',
				'habilitada'    => 1,
				'plataforma'  => 0,
			]);
///////////////////////// Fin Categorias Español////////////////////////////////


		DB::table('categorias')->insert([
				'nombre'     => 'Welcome to C4x Video',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Start',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Definitions and explanations.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 1: Identify technical patterns.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 2: Identifying Structures.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 3: Price action.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 4: Correlation of temporalities.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 5: Fibonacci based on Structure.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Phase 6: Entries recorded live.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);
		DB::table('categorias')->insert([
				'nombre'     => 'Fundamentals.',
				'habilitada'    => 1,
				'plataforma'  => 1,
			]);



	}
}
