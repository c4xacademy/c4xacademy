<?php

use Illuminate\Database\Seeder;

class VistoNotificacionsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('visto_notificacions')->insert([
				'user_id'     => 2,
				'video_id'    => 1,
				'notificado'  => 1,
				'visto'       => 1,
			]);

	}
}
