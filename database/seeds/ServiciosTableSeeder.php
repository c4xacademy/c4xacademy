<?php

use Illuminate\Database\Seeder;

class ServiciosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('servicios')->insert([
				'name'     => 'C4xAcademy (Español)',
				'amount'    => '350',
			]);

		DB::table('servicios')->insert([
				'name'     => 'C4xAcademy (Inglés/English)',
				'amount'    => '350',
			]);

		DB::table('servicios')->insert([
				'name'     => 'Key Level Alerts',
				'amount'    => '27.99',
			]);

		DB::table('servicios')->insert([
				'name'     => 'C4xAcademy (Preview)',
				'amount'    => '0',
			]);


	}
}
