<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('users')->insert([
				'nombre'       => 'C4X Admin',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.admin@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'ADMINISTRADOR',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '0'
			]);
		DB::table('users')->insert([
				'nombre'       => 'C4X User',
				'apellido'     => 'Academy',
				'nickname'     => 'UserC4x',
				'email'        => 'c4xacademy.user@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '0'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User English',
				'apellido'     => 'Academy',
				'nickname'     => 'UserC4x',
				'email'        => 'c4xacademy.user.english@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '1'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User key Level Alerts',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.user.key@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '2'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User Preview',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.user.preview@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '3'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User Español/Inglés',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.user.spanish.english@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '4'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User Español/Key',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.user.spanish.key@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '5'
			]);

		DB::table('users')->insert([
				'nombre'       => 'C4X User English/Key',
				'apellido'     => 'Academy',
				'nickname'     => 'AdminC4x',
				'email'        => 'c4xacademy.user.english.key@gmail.com',
				'password'     => bcrypt('123456'),
				'role'         => 'USUARIO',
				'habilitado'   => 1,
				'edad'         => '25',
				'pago'         => 'TRANSFERENCIA',
				'confirmacion' => '00000000000000000000',
				'plataforma'   => '6'
			]);

	}
}
