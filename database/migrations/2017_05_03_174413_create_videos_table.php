<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('videos', function (Blueprint $table) {
				$table->increments('video_id');
				$table->string('nombre');
				$table->string('descripcion')->nullable();
				$table->integer('posicion')->unsigned()->nullable();
				$table->integer('categoria_id')->unsigned()->nullable();
	            $table->foreign('categoria_id')->references('categoria_id')->on('categorias')->onUpdate('cascade')->onDelete('cascade');
				$table->string('iframe', 1000)->nullable();
				$table->tinyinteger('plataforma')->nullable(); //0 --C4xAcademyEspañol -- 1 -- C4xAcademyEnglish -- 3 -- C4xAcademyPreview		
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('videos');
	}
}
