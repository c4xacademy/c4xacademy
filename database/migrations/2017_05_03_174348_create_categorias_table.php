<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('categorias', function (Blueprint $table) {
				$table->increments('categoria_id');
				$table->string('nombre');
				$table->unsignedTinyInteger('habilitada')->nullable();
				$table->tinyinteger('plataforma')->nullable(); //0 --C4xAcademyEspañol -- 1 -- C4xAcademyInglés -- 3 -- C4xAcademyPreview.
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('categorias');
	}
}
