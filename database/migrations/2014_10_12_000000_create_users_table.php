<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
				$table->increments('id');
				$table->string('nombre', 50);
				$table->string('apellido', 50)->nullable();
				$table->string('nickname')->nullable();
				$table->string('email')->unique();
				$table->string('password');
				$table->string('pais', 100)->nullable();
				$table->string('telefono', 16)->nullable();
				$table->string('telegram')->nullable();
				$table->string('comprobante')->nullable();
				$table->enum('sexo', ['M', 'F'])->nullable();
				$table->enum('role', ['USUARIO', 'ADMINISTRADOR'])->nullable();
				$table->unsignedSmallInteger('edad')->nullable();
				$table->tinyinteger('habilitado')->nullable();
				$table->tinyinteger('habilitado2')->nullable();
				$table->tinyinteger('terminos')->nullable();
				$table->enum('pago', ['BITCOINS', 'TRANSFERENCIA', 'PAYPAL'])->nullable();
				$table->enum('pago2', ['BITCOINS', 'TRANSFERENCIA', 'PAYPAL'])->nullable();
				$table->enum('pago3', ['BITCOINS', 'TRANSFERENCIA', 'PAYPAL'])->nullable();
				$table->string('promocode')->nullable(); //Promocode
				$table->string('confirmacion', 20)->nullable();
				$table->string('confirmacion2', 20)->nullable();
				$table->string('confirmacion3', 20)->nullable();
				$table->tinyinteger('plataforma')->nullable(); //0 --C4xAcademyEspañol -- 1 -- C4xAcademyInglés -- 2 -- KeyLevelAlerts. 3 -- C4xAcademy Preview  ---- 4 español/ingles --- 5 español/key ----- 6 ingles/key 
				$table->tinyinteger('bandera_plataforma')->nullable(); //0 --C4xAcademyEspañol -- 1 -- C4xAcademyInglés -- 2 -- KeyLevelAlerts.
	            $table->string('stripe_id')->nullable();
	            $table->string('card_brand')->nullable();
	            $table->string('card_last_four')->nullable();
	            $table->timestamp('trial_ends_at')->nullable();
				$table->rememberToken();
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
