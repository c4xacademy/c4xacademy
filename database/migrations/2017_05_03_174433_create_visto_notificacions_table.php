<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVistoNotificacionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('visto_notificacions', function (Blueprint $table) {
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
				$table->integer('video_id');
				$table->foreign('video_id')->references('video_id')->on('videos')->onUpdate('cascade')->onDelete('cascade');
				$table->primary(['user_id', 'video_id']);
				$table->boolean('notificado');
				$table->boolean('visto');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('visto_notificacions');
	}
}
