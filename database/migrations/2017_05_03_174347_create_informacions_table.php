<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformacionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('informacions', function (Blueprint $table) {
				$table->increments('id');
				$table->string('nombre')->unique();
				$table->binary('archivo');
				$table->tinyinteger('plataforma')->nullable(); //0 --Profit -- 1 -- Cripto -- 2 -- Ambas.
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('informacions');
	}
}
