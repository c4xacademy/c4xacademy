<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model {
    protected $table = 'archivos';
    protected $primaryKey = 'archivo_id';
    public $timestamps = false;
	protected $fillable = [
		'nombre', 'descripcion', 'ruta_archivo'
	];

}
