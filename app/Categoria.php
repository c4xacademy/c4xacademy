<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {
    protected $table = 'categorias';
    protected $primaryKey = 'categoria_id';
    public $timestamps = false;
	protected $fillable = [
		'nombre', 'habilitada', 'plataforma',
	];

}
