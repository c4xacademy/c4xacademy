<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {
    protected $table = 'sliders';
    protected $primaryKey = 'sliders_cod';

	protected $fillable = [
		'sliders_ruta_imagen', 'sliders_descripcion', 'sliders_descripcion_2', 'sliders_descripcion_3', 'sliders_descripcion_4', 'sliders_descripcion_5',
	];

}
