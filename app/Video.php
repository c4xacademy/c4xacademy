<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {
    protected $table = 'videos';
    protected $primaryKey = 'video_id';
    public $timestamps = false;
	protected $fillable = [
		'nombre', 'descripcion', 'categoria_id', 'iframe', 'plataforma', 'user_id',
	];

}
