<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'servicios';
    public $timestamps = false;
	protected $fillable = [
		'name', 'amount'
	];

}
