<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $table = 'promocodes';
    public $timestamps = false;
	protected $fillable = [
		'code', 'habilitado'
	];

}
