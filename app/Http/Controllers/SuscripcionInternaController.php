<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Illuminate\Auth\Events\Registered;
use DB;
use Auth;    


class SuscripcionInternaController extends Controller
{
    public function pago(Request $request)
    {

        try {
            Stripe::setApiKey(config('services.stripe.secret'));
        $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken
            ));
        $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => 2100,
                'currency' => 'usd'
            ));
        return 'Cargo exitoso!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

    }

    public function vista_suscripcion_interna(Request $request)
    {
	    return view('user_section.espaniol.servicios.suscripcion');
    }

	public function procesa_suscripcion(Request $request)
	{
	    try {

        DB::beginTransaction();
            Stripe::setApiKey(config('services.stripe.secret'));
            $user_sesion = Auth::User();
            $user = User::find($user_sesion->id);
            if($user_sesion->plataforma == 0){
                $user->plataforma = 5;
                $user->save();
            }
            if($user_sesion->plataforma == 1){
                $user->plataforma = 6;
                $user->save();
            }
            if($user_sesion->plataforma == 3){
                $user->plataforma = 2;
                $user->save();
            }
            if($user_sesion->plataforma == 4){
                $user->plataforma = 7;
                $user->save();
            }
            $user->newSubscription('main', 'keylevel-alerts')->create($request->stripeToken);
        DB::commit();
        if (!$user) {
            DB::rollBack();
            return redirect()->route('administrador.archivos')->with('success', 'Error Pagando el Servicio');
        }
            return redirect()->route('login')->with([
                'success' => 'Suscripción exitosa! Acabas de suscribirte a Key Level Alerts de C4xAcademy',
            ]);

	    } catch (\Exception $ex) {
            Auth::logout();
            return redirect()->route('usuario.index')->with('message', $ex->getMessage());
	    }
	}

}
