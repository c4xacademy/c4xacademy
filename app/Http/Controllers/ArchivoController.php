<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;
use Illuminate\Support\Facades\Validator;

class ArchivoController extends Controller {
	//
	public function index() {
		$archivos = Archivo::all();
		return view('admin_section.archivos.index', compact('archivos'));
	}

	public function create() {

		return view('admin_section.archivos.create');
	}

	public function store(Request $request) {
                Validator::make($request->all(), [
                'nombre'   => 'required',
                'ruta_archivo'   => 'required',
                'descripcion'   => 'required',
                    ])->validate();

		DB::beginTransaction();

		$archivo = new Archivo;
		$archivo->nombre = $request->nombre;
		$archivo->ruta_archivo = "";
		$archivo->descripcion = $request->descripcion;
		$archivo->save();

		if (!$archivo) {
			DB::rollBack();
			return redirect()->route('administrador.archivos')->with('success', 'Error Creando Archivo');
		}

		$archivo1 = Archivo::find($archivo->archivo_id);

		$filename = 'archivos/'.$archivo1->archivo_id.'_'.$request->ruta_archivo->getClientOriginalName();

		Storage::disk('imagenes')->put($filename, File::get($request->ruta_archivo));

		$route = 'imagenes/'.$filename;

		$archivo1->ruta_archivo = $route;

		$archivo1->save();

		if (!$archivo1) {
			DB::rollBack();
			return redirect()->route('administrador.archivos')->with('success', 'Error Creando Archivo');
		}

		DB::commit();

			return redirect()->route('administrador.archivos')->with('success', 'Exito Creando Archivo');

	}

	public function edit($id) {
		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}
		$archivo = Archivo::find($d)->first();
		return view('admin_section.archivos.edit', compact('archivo'));

	}

	public function update(Request $request) {
		$archivo = Archivo::find($request->id);

		if (Input::hasFile('ruta_archivo')) {
			if (Input::file('ruta_archivo')->isValid()) {

				Storage::disk('imagenes')->delete(substr($archivo->ruta_archivo, strpos($archivo->ruta_archivo, '/'), strlen($archivo->ruta_archivo)));

				DB::beginTransaction();

				$archivo->nombre = $request->nombre;
				$archivo->descripcion = $request->descripcion;

				$filename = 'archivos/'.$archivo->archivo_id.'_'.$request->ruta_archivo->getClientOriginalName();

				Storage::disk('imagenes')->put($filename, File::get($request->ruta_archivo));

				$route = 'imagenes/'.$filename;

				$archivo->ruta_archivo = $route;

				$archivo->save();

				if (!$archivo) {
					$id = Crypt::encrypt($archivo->archivo_id);
					DB::rollBack();
					return redirect()->route('administrador.archivos')->with('success', 'Error Modificando Archivo');
				}

				DB::commit();
			}
		} else {

				$archivo->nombre = $request->nombre;
				$archivo->descripcion = $request->descripcion;
			$archivo->save();
		}

			return redirect()->route('administrador.archivos')->with('success', 'Exito Modificando Archivo');

	}

    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $archivo = Archivo::findOrfail($d);
     	$archivo->delete();
			return redirect()->route('administrador.archivos')->with('success', 'Exito Eliminando Archivo');
    }




}
