<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\User;
use App\Categoria;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller {
	//
	public function index() {
        $videos = DB::table('videos')
                     ->join('categorias', 'categorias.categoria_id', '=', 'videos.categoria_id')
                     ->select('videos.*', 'categorias.nombre as nombre_categoria')
                     ->get();

		return view('admin_section.videos.index', compact('videos'));
	}

	public function create() {
		$categorias = Categoria::pluck('nombre', 'categoria_id');
		return view('admin_section.videos.create', compact('categorias'));
	}

	public function store(Request $request) {
                Validator::make($request->all(), [
                'nombre'   => 'required',
                'descripcion'   => 'required',
                'iframe'   => 'required',
                'plataforma'   => 'required',
                'categoria_id'   => 'required',
                'posicion'   => 'required',
                    ])->validate();
		DB::beginTransaction();

		$video = new Video;
		$video->nombre = $request->nombre;
		$video->descripcion = $request->descripcion;
		$video->iframe = $request->iframe;
		$video->categoria_id = $request->categoria_id;
		$video->plataforma = $request->plataforma;
		$video->posicion = $request->posicion;
		$video->save();

		$users = User::where('plataforma', '=', $request->plataforma)->get();

		foreach ($users as $user) {
			DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
		}

		if (!$video) {
			DB::rollBack();
			DB::delete('delete from visto_notificacions where user_id = :id', ['id' => $users->id]);
			return redirect()->route('administrador.videos')->with('success', 'Error Creando Video');
		}
		DB::commit();
			return redirect()->route('administrador.videos')->with('success', 'Exito Creando Video');
	}

	public function edit($id) {
		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}
		$video = Video::where('video_id', '=', $d)->first();
		$categorias = Categoria::pluck('nombre', 'categoria_id');

		$categoria = Categoria::where('categoria_id', '=', $video->categoria_id)->get()->first();
		return view('admin_section.videos.edit', compact('video', 'categorias', 'categoria'));

	}

	public function update(Request $request) {
		$video = Video::find($request->id);

				DB::beginTransaction();

				$video->nombre = $request->nombre;
				$video->descripcion = $request->descripcion;
				$video->iframe = $request->iframe;
				$video->categoria_id = $request->categoria_id;
				$video->plataforma = $request->plataforma;
				$video->posicion = $request->posicion;
				$video->save();

				if (!$video) {
					$id = Crypt::encrypt($video->video_id);
					DB::rollBack();
					return redirect()->route('administrador.videos')->with('success', 'Error Modificando Video');
				}
				DB::commit();
			return redirect()->route('administrador.videos')->with('success', 'Exito Modificando Video');
	}

    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $video = Video::findOrfail($d);
     	$video->delete();
			return redirect()->route('administrador.videos')->with('success', 'Exito Eliminando Video');
    }


	public function listado_videos_categoria(Request $request, $id) {

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$categoria = Categoria::where('categoria_id', '=', $d)->first();
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
		$videos = Video::where('categoria_id', '=', $d)->where('plataforma', '=', $plataforma_sesion)->orderBy('posicion', 'DESC')->get();

		return view('user_section.espaniol.videos.index', compact('videos', 'categorias', 'categoria'));
	}

	public function listado_videos_categoria_english(Request $request, $id) {

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$categoria = Categoria::where('categoria_id', '=', $d)->first();
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
		$videos = Video::where('categoria_id', '=', $d)->where('plataforma', '=', $plataforma_sesion)->orderBy('posicion', 'DESC')->get();

		return view('user_section.english.videos.index', compact('videos', 'categorias', 'categoria'));
	}

	public function listado_videos_categoria_preview(Request $request, $id) {

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$categoria = Categoria::where('categoria_id', '=', $d)->first();
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
		$videos = Video::where('categoria_id', '=', $d)->where('plataforma', '=', $plataforma_sesion)->orderBy('posicion', 'DESC')->get();

		return view('user_section.preview.videos.index', compact('videos', 'categorias', 'categoria'));
	}


	public function video_reproduccion(Request $request, $id) {
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$video = Video::where('video_id', '=', $d)->first();

		DB::update('update visto_notificacions set visto = true, notificado = true where user_id = :id and video_id = :video_id', ['id' => Auth::user()->id, 'video_id' => $video->video_id]);

		return view('user_section.espaniol.videos.video', compact('categorias', 'video'));
	}

	public function video_reproduccion_english(Request $request, $id) {
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$video = Video::where('video_id', '=', $d)->first();

		DB::update('update visto_notificacions set visto = true, notificado = true where user_id = :id and video_id = :video_id', ['id' => Auth::user()->id, 'video_id' => $video->video_id]);

		return view('user_section.english.videos.video', compact('categorias', 'video'));
	}

	public function video_reproduccion_preview(Request $request, $id) {
        $plataforma_sesion = $request->session()->get('plataforma.data');
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();

		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}

    	$video = Video::where('video_id', '=', $d)->first();

		DB::update('update visto_notificacions set visto = true, notificado = true where user_id = :id and video_id = :video_id', ['id' => Auth::user()->id, 'video_id' => $video->video_id]);

		return view('user_section.preview.videos.video', compact('categorias', 'video'));
	}

}
