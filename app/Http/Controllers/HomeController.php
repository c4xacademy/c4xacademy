<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        return view('select');
    }



    public function select_store(Request $request) {

        $request->session()->put('plataforma.data', $request->plataforma); // guardamos usuario en session
        switch($request->plataforma){
            case '0':
                return redirect('/usuario');
            break;
            case '1':
                return redirect('/user');
            break;
            case '2':
                return redirect('/key_level_alerts');
            break;
            case '3':
                return redirect('/user_preview');
            break;
                default:
                break;
        }
    }


}
