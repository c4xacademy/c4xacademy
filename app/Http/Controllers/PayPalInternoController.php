<?php

namespace App\Http\Controllers;

use App\Order;
use App\Helpers\PayPalInterno;
use App\Service;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Illuminate\Auth\Events\Registered;

/**
 * Class PayPalController
 * @package App\Http\Controllers
 */
class PayPalInternoController extends Controller
{
    
    
    public function form(Request $request, $service_id)
    {       
            Auth::logout();
            return redirect()->route('register')->with([
                'success' => 'No se pudo completar el pago ni el registro.',
            ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function checkout(Request $request, $transaction_id)
    {
            $user_sesion = $request->session()->get('C4xAcademy.user_interno.data');

            $order = new Order;
            $order->user_name = Auth::User()->name;  //user id
            $order->transaction_id = $transaction_id;

            if($user_sesion['bandera_plataforma'] == 0){
                $order->service_id    = 1;                
                $service = Service::findOrFail(1);
            }
            if($user_sesion['bandera_plataforma'] == 1){
                $order->service_id    = 2;                
                $service = Service::findOrFail(2);
            }
            $order->amount  = $service->amount;
            $order->save();


            $paypal = new PayPalInterno;

            $response = $paypal->purchase([
                'amount' => $paypal->formatAmount($order->amount),
                'transactionId' => $order->transaction_id,
                'currency' => 'USD',
                'cancelUrl' => $paypal->getCancelUrl($order),
                'returnUrl' => $paypal->getReturnUrl($order),
            ]);


            if ($response->isRedirect()) {
                $response->redirect();
            }

            return redirect()->back()->with([
                 'success' => $response->getMessage(),
                
            ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     * @return mixed
     */
    public function completed($order_id, Request $request)
    {

        $order = Order::findOrFail($order_id);

        $paypal = new PayPalInterno;

        $response = $paypal->complete([
            'amount' => $paypal->formatAmount($order->amount),
            'transactionId' => $order->transaction_id,
            'currency' => 'USD',
            'cancelUrl' => $paypal->getCancelUrl($order),
            'returnUrl' => $paypal->getReturnUrl($order),
            'notifyUrl' => $paypal->getNotifyUrl($order),
        ]);

        if ($response->isSuccessful()) {
            $order->update([
                'transaction_id' => $response->getTransactionReference(),
                'payment_status' => Order::PAYMENT_COMPLETED,
            ]);

            return redirect()->route('paymentCompleted_interno', encrypt($order_id))->with([
                'message' => 'You recent payment is sucessful with reference code ' . $response->getTransactionReference(),
            ]);
        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);

    }
    public function paymentCompleted_interno(Request $request, $order)
    {
            $user_sesion = $request->session()->get('C4xAcademy.user_interno.data');
            $user = Auth::User();
            if($user->plataforma == 0){
                if($user_sesion['bandera_plataforma'] == 1){
                    $user->plataforma = 4;                    
                    $user->update();
                }
            }
            if($user->plataforma == 1){
                if($user_sesion['bandera_plataforma'] == 0){
                    $user->plataforma = 4;                    
                    $user->update();
                }
            }
            if($user->plataforma == 2){
                if($user_sesion['bandera_plataforma'] == 0){
                    $user->plataforma = 5;                    
                    $user->update();
                }
                if($user_sesion['bandera_plataforma'] == 1){
                    $user->plataforma = 6;                    
                    $user->update();
                }
            }
            if($user->plataforma == 3){
                if($user_sesion['bandera_plataforma'] == 0){
                    $user->plataforma = 0;                    
                    $user->update();
                }
                if($user_sesion['bandera_plataforma'] == 1){
                    $user->plataforma = 1;                    
                    $user->update();
                }
                if($user_sesion['bandera_plataforma'] == 2){
                    $user->plataforma = 2;                    
                    $user->update();
                }
            }
            if($user->plataforma == 4){
                if($user_sesion['bandera_plataforma'] == 2){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }
            if($user->plataforma == 5){
                if($user_sesion['bandera_plataforma'] == 1){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }
            if($user->plataforma == 6){
                if($user_sesion['bandera_plataforma'] == 0){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }

                $videos = Video::where('plataforma', '=', $user_sesion['bandera_plataforma'])->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }


            return redirect()->route('login')->with([
                'success' => 'Pago realizado correctamente, usuario creado y habilitado. Puede acceder a la plataforma.',
            ]);


    }

    /**
     * @param $order_id
     */
    public function cancelled($order_id)
    {
        $order = Order::findOrFail($order_id);
        Auth::logout();
        return redirect()->route('order.paypal.interno', encrypt($order_id))->with([
            'message' => 'You have cancelled your recent PayPal payment !',
        ]);
    }

        /**
     * @param $order_id
     * @param $env
     * @param Request $request
     */
    public function webhook($order_id, $env, Request $request)
    {
        // to do with new release of sudiptpa/paypal-ipn v3.0 (under development)
    }
    
}