<?php

namespace App\Http\Controllers\Auth;

use App\Order;
use App\Service;
use App\User;
use App\Video;
use DB;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Storage;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
                'nombre'   => 'required|max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'required|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'required|alpha_num|max:191',
                'email'    => 'required|string|email|max:191|unique:users,email',
                'password' => 'required|string|min:5|confirmed|max:191',
                'pais'     => 'required|max:100|nullable|present',
                'telefono'      => 'required|max:15|nullable|present|regex:[\+?[0-9]{9,14}]',
                'sexo'     => 'required',
                'edad'     => 'required|numeric|between:0,120|nullable|present',
                'plataforma' => 'required',
                'terminos' => 'required',
                'confirmacion',
                'telegram',
                'promocode',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
                    'nombre'       => $data['nombre'],
                    'apellido'     => $data['apellido'],
                    'nickname'     => $data['nickname'],
                    'email'        => $data['email'],
                    'password'     => Hash::make($data['password']),
                    'pais'         => $data['pais'],
                    'telefono'     => $data['telefono'],
                    'sexo'         => $data['sexo'],
                    'role'         => 'USUARIO',
                    'habilitado'   => 0,
                    'edad'         => $data['edad'],
                    'pago'         => $data['pago'],
                    'plataforma'   => $data['plataforma'],
                    'terminos'     => $data['terminos'],
                    'confirmacion' => $data['confirmacion'],
                    'telegram'     => $data['telegram'],
	                'promocode'	   => $data['promocode'],
        ]);
    }

    public function register(Request $request) {

        $this->validator($request->all())->validate();

        if(($request->pago == 'TRANSFERENCIA' || $request->pago == 'BITCOINS') && ($request->plataforma != '3') ){
                Validator::make($request->all(), [
                        'confirmacion' => 'required|max:100|string|alpha_num',
                        'pago' => 'required',
                    ])->validate();

            event(new Registered($user = $this->create($request->all())));

            if($request->plataforma == 0){
                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }
            }
            if($request->plataforma == 1){
                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }
            }




            $mensaje = "Usuario Creado Exitosamente, por favor espere que su cuenta sea habilitada.";
            return $this->registered($request, $user)
            ?:redirect($this->redirectPath())->withInput()->with('success', $mensaje);
        }

        if($request->plataforma == '3'){

            $user = User::create([
                    'nombre'       => $request->nombre,
                    'apellido'     => $request->apellido,
                    'nickname'     => $request->nickname,
                    'email'        => $request->email,
                    'password'     => Hash::make($request->password),
                    'pais'         => $request->pais,
                    'telefono'     => $request->telefono,
                    'sexo'         => $request->sexo,
                    'role'         => 'USUARIO',
                    'habilitado'   => 1,
                    'edad'         => $request->edad,
                    'plataforma'   => $request->plataforma,
                    'terminos'     => $request->terminos,
            ]);

            event(new Registered($user));

                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }


            $mensaje = "Usuario para C4xAcademy Preview Creado Exitosamente";
            return $this->registered($request, $user)
            ?:redirect($this->redirectPath())->withInput()->with('success', $mensaje);
        }



        if($request->plataforma == '2'){
                Validator::make($request->all(), [
                        'telegram' => 'required',
                    ])->validate();

             $arreglo = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'edad' => $request->edad, 'pais' => $request->pais, 'telefono' => $request->telefono, 'nickname' => $request->nickname, 'sexo' => $request->sexo, 'plataforma' => $request->plataforma, 'pago' => $request->pago, 'role' => 'USUARIO', 'email' => $request->email, 'password' => bcrypt($request->password), 'telegram' => $request->telegram];

            $request->session()->put('C4xAcademy.user.data', $arreglo); // guardamos usuario en session

           return redirect()->route('stripe.suscripcion');
        }


/* Paypal Real
        if(($request->pago == 'PAYPAL') && ($request->plataforma != '3') ){
            $transaction_id = rand(10000000,99999999);
            $arreglo = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'edad' => $request->edad, 'pais' => $request->pais, 'telefono' => $request->telefono, 'nickname' => $request->nickname, 'sexo' => $request->sexo, 'plataforma' => $request->plataforma, 'pago' => $request->pago, 'role' => 'USUARIO', 'email' => $request->email, 'password' => bcrypt($request->password), 'promocode' => $request->promocode];

            $request->session()->put('C4xAcademy.user.data', $arreglo); // guardamos usuario en session


           return redirect()->route('checkout.payment.paypal', ['transaction_id' => $transaction_id]);
        }
*/


        if(($request->pago == 'PAYPAL') && ($request->plataforma != '3') ){

                Validator::make($request->all(), [
                        'comprobante' => 'required|file',
                    ])->validate();


            $user = User::create([
                    'nombre'       => $request->nombre,
                    'apellido'     => $request->apellido,
                    'nickname'     => $request->nickname,
                    'email'        => $request->email,
                    'password'     => Hash::make($request->password),
                    'pais'         => $request->pais,
                    'telefono'     => $request->telefono,
                    'sexo'         => $request->sexo,
                    'role'         => 'USUARIO',
                    'habilitado'   => 0,
                    'edad'         => $request->edad,
                    'plataforma'   => $request->plataforma,
                    'terminos'     => $request->terminos,
                    'pago'     	   => $request->pago,
            ]);



		$filename = 'comprobantes_paypal/'.$user->id.'_'.$request->comprobante->getClientOriginalName();

		Storage::disk('imagenes')->put($filename, File::get($request->comprobante));

		$route = 'imagenes/'.$filename;

		$user->comprobante = $route;

		$user->save();





            event(new Registered($user));

                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }





            $mensaje = "Usuario Creado Exitosamente, por favor espere que su cuenta sea habilitada.";
            return $this->registered($request, $user)
            ?:redirect($this->redirectPath())->withInput()->with('success', $mensaje);
        }





        dd($request->all());
    }



}
