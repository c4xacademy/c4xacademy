<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/select';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


	public function redirectTo(){
	        
	    // User role
	    $role = Auth::user()->role; 
		    if(Auth::user()->habilitado == 1){
		    // Check user role
			    switch ($role) {
			        case 'ADMINISTRADOR':
			                return '/administrador';
			            break;
			        case 'USUARIO':
			                return '/select';
			            break; 
			        default:
			            break;
			    }
	    	}
	    	else{
                $mensaje = "Por favor espere a que su cuenta sea activada.";
			                return '/login';
	    	}
	}
}
