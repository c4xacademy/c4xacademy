<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller {
	//
	public function index() {
		$sliders = Slider::all();
		return view('admin_section.sliders.sliders', compact('sliders'));
	}

	public function create() {

		return view('admin_section.sliders.create');
	}

	public function store(Request $request) {
                Validator::make($request->all(), [
                'sliders_ruta_imagen'   => 'required',
                    ])->validate();
		DB::beginTransaction();

		$slider = new Slider;

		$slider->sliders_descripcion = $request->descripcion;
		$slider->sliders_descripcion_2 = $request->descripcion_2;
		$slider->sliders_ruta_imagen = "";

		$slider->save();

		if (!$slider) {

			DB::rollBack();
			return redirect()->route('administrador.sliders')->with('success', 'Error Creando Slider');
		}

		$slider1 = Slider::find($slider->sliders_cod);

		$filename = 'sliders/'.$slider1->sliders_cod.'_'.$request->sliders_ruta_imagen->getClientOriginalName();

		Storage::disk('imagenes')->put($filename, File::get($request->sliders_ruta_imagen));

		$route = 'imagenes/'.$filename;

		$slider1->sliders_ruta_imagen = $route;

		$slider1->save();

		if (!$slider1) {
			DB::rollBack();
			return redirect()->route('administrador.sliders')->with('success', 'Error Creando Slider');
		}

		DB::commit();

			return redirect()->route('administrador.sliders')->with('success', 'Exito Creando Slider');

	}

	public function edit($id) {
		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}
		$slider = Slider::find($d)->first();
		return view('admin_section.sliders.edit', compact('slider'));

	}

	public function update(Request $request) {
		$slider = Slider::find($request->id);

		if (Input::hasFile('sliders_ruta_imagen')) {
			if (Input::file('sliders_ruta_imagen')->isValid()) {

				Storage::disk('imagenes')->delete(substr($slider->sliders_ruta_imagen, strpos($slider->sliders_ruta_imagen, '/'), strlen($slider->sliders_ruta_imagen)));

				DB::beginTransaction();

				$slider->sliders_descripcion = $request->descripcion;
				$slider->sliders_descripcion_2 = $request->descripcion_2;

				$filename = 'sliders/'.$slider->sliders_cod.'_'.$request->sliders_ruta_imagen->getClientOriginalName();

				Storage::disk('imagenes')->put($filename, File::get($request->sliders_ruta_imagen));

				$route = 'imagenes/'.$filename;

				$slider->sliders_ruta_imagen = $route;

				$slider->save();

				if (!$slider) {
					$id = Crypt::encrypt($slider->sliders_cod);
					DB::rollBack();
					return redirect()->route('administrador.sliders')->with('success', 'Error Modificando Slider');
				}

				DB::commit();
			}
		} else {

			$slider->sliders_descripcion = $request->descripcion;
			$slider->sliders_descripcion_2 = $request->descripcion_2;
			$slider->save();
		}

			return redirect()->route('administrador.sliders')->with('success', 'Exito Modificando Slider');

	}

    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $slider = Slider::findOrfail($d);
     	$slider->delete();
			return redirect()->route('administrador.sliders')->with('success', 'Exito Eliminando Slider');
    }




}
