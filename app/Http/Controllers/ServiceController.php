<?php

namespace App\Http\Controllers;

use App\Service;
use App\Categoria;
use Illuminate\Http\Request;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Service::all();

        return view('admin_section.servicios.index', compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin_section.servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'name'   => 'required',
            'amount'   => 'required',
        ])->validate();

        DB::beginTransaction();

        $servicio = new Service;
        $servicio->name = $request->name;
        $servicio->amount = $request->amount;
        $servicio->save();

        DB::commit();

        if (!$servicio) {
            DB::rollBack();
            return redirect()->route('administrador.servicios')->with('success', 'Error Creando Servicio');
        }

        return redirect()->route('administrador.servicios')->with('success', 'Exito Creando Servicio');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }
        $servicio = Service::find($d)->first();
        return view('admin_section.servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::beginTransaction();

        $servicio = Service::find($request->id);
        $servicio->name = $request->name;
        $servicio->amount = $request->amount;
        $servicio->update();

        DB::commit();

        if (!$servicio) {
            DB::rollBack();
            return redirect()->route('administrador.servicios')->with('success', 'Error Actualizando Servicio');
        }

        return redirect()->route('administrador.servicios')->with('success', 'Exito Actualizando Servicio');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $servicios = Service::findOrfail($d);
        $servicios->delete();

        return redirect()->route('administrador.servicios')->with('success', 'Exito Eliminando Servicio');
    }

    public function adquirir(Request $request)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        $categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        $servicios = Service::all();
        return view('user_section.espaniol.servicios.servicio', compact('servicios', 'categorias'));
    }

    public function adquirir_preview(Request $request)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        $categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        $servicios = Service::all();
        return view('user_section.preview.servicios.servicio', compact('servicios', 'categorias'));
    }

    public function adquirir_key(Request $request)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        $categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        $servicios = Service::all();
        return view('user_section.key_level_alerts.servicios.servicio', compact('servicios', 'categorias'));
    }

    public function buy(Request $request)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        $categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        $servicios = Service::all();
        return view('user_section.english.services.service', compact('servicios', 'categorias'));
    }

    public function store_adquirir(Request $request)
    {
        if($request->plataforma == 2){
           return redirect()->route('stripe.suscripcion_interna');
        }

        if($request->pago == 'PAYPAL'){
           $transaction_id = rand(10000000,99999999);
           $arreglo = ['plataforma' => $request->plataforma, 'pago' => $request->pago, 'bandera_plataforma' => $request->plataforma];
           $request->session()->put('C4xAcademy.user_interno.data', $arreglo); // guardamos usuario en session

           return redirect()->route('checkout.payment.paypal.interno', ['transaction_id' => $transaction_id]);           
        }

        if($request->pago == 'TRANSFERENCIA' || $request->pago == 'BITCOINS'){
            DB::beginTransaction();
            $plataforma_resp = Auth::User()->plataforma;

            $user = Auth::User();
            $user->pago2 = $request->pago;
            $user->habilitado = 0;
            $user->bandera_plataforma = $plataforma_resp;
            if(Auth::User()->plataforma == 0){
                if($request->plataforma == 1){
                    $user->plataforma = 4;                    
                    $user->update();
                }
                if($request->plataforma == 2){
                    $user->plataforma = 5;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 1){
                if($request->plataforma == 0){
                    $user->plataforma = 4;                    
                    $user->update();
                }
                if($request->plataforma == 2){
                    $user->plataforma = 6;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 2){
                if($request->plataforma == 0){
                    $user->plataforma = 5;                    
                    $user->update();
                }
                if($request->plataforma == 1){
                    $user->plataforma = 6;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 3){
                if($request->plataforma == 0){
                    $user->plataforma = 0;                    
                    $user->update();
                }
                if($request->plataforma == 1){
                    $user->plataforma = 1;                    
                    $user->update();
                }
                if($request->plataforma == 2){
                    $user->plataforma = 2;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 4){
                if($request->plataforma == 2){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 5){
                if($request->plataforma == 1){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }
            if(Auth::User()->plataforma == 6){
                if($request->plataforma == 0){
                    $user->plataforma = 7;                    
                    $user->update();
                }
            }
            $user->confirmacion = $request->confirmacion;
            $user->update();
            DB::commit();

            Auth::logout();
            return redirect()->route('login')->with('success', 'Por favor espere que su nuevo servicio sea habilitado.');            

            if (!$user) {
                DB::rollBack();
                Auth::logout();
                return redirect()->route('login')->with('success', 'Error Creando Usuario');
            }

        }


















    }

}
