<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Slider;
use App\Service;
use App\Video;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller {
	//
	public function inicial() {
		$users = User::where('role', '=', 'USUARIO')->where('habilitado', '=', 1)->count();
		$users_key = User::where('plataforma', '=', 2)->orWhere('plataforma', '=', 5)->orWhere('plataforma', '=', 6)->orWhere('plataforma', '=', 7)->where('habilitado', '=', 1)->count();
		$sliders = Slider::all();
        $videos_espaniol = Video::where('plataforma', '=', 0)->count();	
        $videos_ingles = Video::where('plataforma', '=', 1)->count();	 

		return view('index', compact('sliders', 'videos_espaniol', 'users', 'users_key','videos_ingles'));
	}
	public function servicios() {
		$sliders = Slider::all();
		$servicios = Service::all();
		return view('servicios', compact('sliders', 'servicios'));
	}
}
