<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;

class CategoriaController extends Controller {
	//
	public function index() {
		$categorias = Categoria::all();
		return view('admin_section.categorias.index', compact('categorias'));
	}

	public function create() {

		return view('admin_section.categorias.create');
	}

	public function store(Request $request) {
		DB::beginTransaction();

		$categoria = new Categoria;
		$categoria->nombre = $request->nombre;
		$categoria->habilitada = 0;
		$categoria->plataforma = $request->plataforma;
		$categoria->save();

		if (!$categoria) {

			DB::rollBack();
			return redirect()->route('administrador.categorias')->with('success', 'Error Creando Categoria');
		}
		DB::commit();
			return redirect()->route('administrador.categorias')->with('success', 'Exito Creando Categoria');
	}

	public function edit($id) {
		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}
    	$categoria = Categoria::where('categoria_id', '=', $d)->first();
		return view('admin_section.categorias.edit', compact('categoria'));

	}

	public function update(Request $request) {
		$categoria = Categoria::find($request->id);

				DB::beginTransaction();

				$categoria->nombre = $request->nombre;
				$categoria->plataforma = $request->plataforma;
				$categoria->save();

				if (!$categoria) {
					$id = Crypt::encrypt($categoria->categoria_id);
					DB::rollBack();
					return redirect()->route('administrador.categorias')->with('success', 'Error Modificando Categoria');
				}
				DB::commit();
			return redirect()->route('administrador.categorias')->with('success', 'Exito Modificando Categoria');
	}

    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $categoria = Categoria::findOrfail($d);
     	$categoria->delete();
			return redirect()->route('administrador.categorias')->with('success', 'Exito Eliminando Categoria');
    }

    public function EstadoCategoria($id)
    {
        try {
            $categoria_id = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $categoria = Categoria::findOrfail($categoria_id);

        if ($categoria->habilitada == '0' || $categoria->habilitada == null) {
            $categoria->habilitada = '1';
            $categoria->save();
        }
        else{
            $categoria->habilitada = '0';
            $categoria->save();           
        }

        return redirect()->route('administrador.categorias')->with('success', 'Categoria  Habilitada Exitosamente');

    }




}
