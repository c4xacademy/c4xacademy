<?php

namespace App\Http\Controllers;

use App\Mail\RegistroUsuario;
use Auth;    
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Video;
use App\Categoria;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Crypt;
use Illuminate\Support\Facades\Storage;
use Mail;


class AdministradorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $cuenta_videos = DB::table('videos')
                ->select(DB::raw('count(*) as videos_total'))
                ->first();
        $cuenta_usuarios_habilitados = DB::table('users')
                ->where('users.habilitado', '=', '1')
                ->where('users.role', '=', 'USUARIO')
                ->select(DB::raw('count(*) as usuarios_habilitados'))
                ->first();
        $cuenta_usuarios_no_habilitados = DB::table('users')
                ->where('users.habilitado', '=', '0')
                ->where('users.role', '=', 'USUARIO')
                ->select(DB::raw('count(*) as usuarios_no_habilitados'))
                ->first();
        $categorias = DB::table('categorias')
                ->select(DB::raw('count(*) as categorias_cuenta'))
                ->first();


        return view('admin_section.index', compact('cuenta_videos', 'cuenta_usuarios_habilitados', 'cuenta_usuarios_no_habilitados', 'categorias'));
    }

    public function perfil(){
        $administrador = Auth::user();
        return view('admin_section.perfil', compact('administrador'));
    }

    public function update_perfil(Request $request) {
 
        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();


        if ($user->count() != 0) {
            if ($user[0]->id != Auth::user()->id) {
                Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ])->validate();
            }
        }


                Validator::make($request->all(), [
                'nombre'   => 'max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'alpha_num|max:191',
                'password' => 'confirmed',
                'pais'     => 'max:30|nullable|present',
                'telefono'      => 'max:15|nullable|present|regex:[\+?[0-9]{9,14}]',
                'edad'     => 'numeric|between:0,120|nullable|present',

                    ])->validate();


        $usuario = User::find(Auth::user()->id);

        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->edad     = $request->edad;
        $usuario->pais     = $request->pais;
        $usuario->telefono = $request->telefono;
        $usuario->nickname = $request->nickname;

        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }

        $usuario->save();

        $mensaje = "Usuario Modificado Exitosamente";
        return Redirect::route('administrador')->withInput()->with('success', $mensaje);
    }





    public function listadoUsuarios(Request $request) {


        $users = DB::table('users')
            ->select('users.*') ->where('role', '=', 'USUARIO')
            ->orderByRaw('updated_at - created_at ASC')
            ->where('habilitado', '=', 1)->get();

        return view('admin_section.usuarios.index', compact('users'));
    }

    public function listadoUsuariosKey(Request $request) {


        $users = DB::table('users')
            ->select('users.*')->where('role', '=', 'USUARIO')
            ->where('habilitado', '=', 1)
            ->where('plataforma', '=', 2)
            ->orWhere('plataforma', '=', 5)
            ->orWhere('plataforma', '=', 6)
            ->orWhere('plataforma', '=', 7)
            ->get();

        return view('admin_section.key_level_alerts.index', compact('users'));
    }


    public function listadoUsuarioshabilitar(Request $request) {

        $users = DB::table('users')
            ->select('users.*')
            ->where('role', '=', 'USUARIO')
            ->where('habilitado', '=', 0)
            ->orWhere('habilitado2', '!=', 0)
            ->get();

        return view('admin_section.usuarios.usuarios_habilitar.index', compact('users'));
    }

    public function habilitarUsuarioAdministrador($id) {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::where('id', '=', $d)->first();
        
        DB::update('update users set habilitado = 1 where id = :id ', ['id' => $d]);

            try {
                Mail::to($usuario->email)->send(new RegistroUsuario($usuario));
            } catch (\Exception $ex) {
                $mensaje = "Usuario Habilitado Exitosamente, no se envió el correo.";
                return Redirect::route('administrador.listadousuarioshabilitar')->withInput()->with('success', $ex->getMessage());
            }

        $mensaje = "Usuario Habilitado Exitosamente";
        return Redirect::route('administrador.listadousuarioshabilitar')->withInput()->with('success', $mensaje);
    }
    public function deshabilitarUsuarioAdministrador($id) {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::where('id', '=', $d)->first();

        DB::update('update users set habilitado = 1 where id = :id ', ['id' => $d]);
        DB::update('update users set plataforma = :plataforma where id = :id ', ['id' => $d, 'plataforma' => $usuario->bandera_plataforma]);
        DB::update('update users set bandera_plataforma = null where id = :id ', ['id' => $d]);


        $mensaje = "Usuario DesHabilitado Exitosamente";

        return Redirect::route('administrador.listadousuarioshabilitar')->withInput()->with('success', $mensaje);
    }



    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::findOrfail($d);
        $usuario->delete();
            return redirect()->route('administrador.listadousuarios')->with('success', 'Exito Eliminando Usuario');
    }

    public function create(){
        return view('admin_section.usuarios.create');
    }


    public function edit($id){
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::find($d);
        return view('admin_section.usuarios.edit', compact('usuario'));
    }


    public function store_administrador(Request $request) {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0) {
            if ($user[0]->id != $request->id) {
                Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ])->validate();
            }
        }

                Validator::make($request->all(), [
                'nombre'   => 'max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'alpha_num|max:191',
                'password' => 'required|confirmed',
                'pais'     => 'max:80|nullable|present',
                'telefono'      => 'max:15|nullable|present|regex:[\+?[0-9]{9,14}]',
                'edad'     => 'numeric|between:0,120|nullable|present',

                    ])->validate();


        $usuario = new User();
        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->edad     = $request->edad;
        $usuario->pais     = $request->pais;
        $usuario->telefono = $request->telefono;
        $usuario->nickname = $request->nickname;
        $usuario->plataforma = $request->plataforma;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->habilitado = 1;
        $usuario->role = 'USUARIO';

        $usuario->save();


            if($request->plataforma == 0){
                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$usuario->id, $video->video_id, '1', '0']);
                }
            }
            if($request->plataforma == 1){
                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$usuario->id, $video->video_id, '1', '0']);
                }
            }
            if($request->plataforma == 3){
                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$usuario->id, $video->video_id, '1', '0']);
                }
            }




        $mensaje = "Usuario Creado Exitosamente";
        return Redirect::route('administrador.listadousuarios')->withInput()->with('success', $mensaje);
    }




    public function update_administrador(Request $request) {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0) {
            if ($user[0]->id != $request->id) {
                Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ])->validate();
            }
        }

                Validator::make($request->all(), [
                'nombre'   => 'max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'alpha_num|max:191',
                'password' => 'confirmed',
                'pais'     => 'max:80|nullable|present',
                'telefono'      => 'max:15|nullable|present|regex:[\+?[0-9]{9,14}]',
                'edad'     => 'numeric|between:0,120|nullable|present',

                    ])->validate();


        $usuario = User::find($request->id);

        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->edad     = $request->edad;
        $usuario->pais     = $request->pais;
        $usuario->telefono = $request->telefono;
        $usuario->nickname = $request->nickname;
        $usuario->plataforma = $request->plataforma;

        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }

        $usuario->save();

        $mensaje = "Usuario Modificado Exitosamente";
        return Redirect::route('administrador.listadousuarios')->withInput()->with('success', $mensaje);
    }









}
