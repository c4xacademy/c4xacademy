<?php

namespace App\Http\Controllers;
use App\User;
use App\Categoria;
use App\Archivo;
use App\Video;
use DB;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Validator;

class UserKeyLevelAlertsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */


    public function index(Request $request) {
        $plataforma_sesion = $request->session()->get('plataforma.data');
//        $request->session()->forget('plataforma.data');

        return view('user_section.key_level_alerts.index');
    }


    public function perfil(Request $request, $id)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        try {
            $usuario_id = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::findOrfail($usuario_id)->first();

        return view('user_section.key_level_alerts.perfil_usuario.perfil', compact('usuario'));

    }

    public function update_perfil(Request $request) {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();


        if ($user->count() != 0) {
            if ($user[0]->id != Auth::user()->id) {
                Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ])->validate();
            }
        }


                Validator::make($request->all(), [
                'nombre'   => 'max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'alpha_num|max:191',
                'password' => 'confirmed',
                'pais'     => 'max:30|nullable|present',
                'telefono'      => 'max:15|nullable|present|regex:[\+?[0-9]{9,14}]',

                    ])->validate();


        $usuario = User::find(Auth::user()->id);

        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->pais     = $request->pais;
        $usuario->telefono = $request->telefono;
        $usuario->nickname = $request->nickname;

        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }

        $usuario->save();

        $mensaje = "User Updated";
        return redirect()->route('key_level_alerts.perfil', ['id' => Crypt::encrypt($usuario->id)])->withInput()->with('success', $mensaje);

    }

}
