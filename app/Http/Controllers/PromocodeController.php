<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Promocode;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Storage;

class PromocodeController extends Controller {
	//
	public function index() {
		$promocodes = Promocode::all();
		return view('admin_section.promocodes.index', compact('promocodes'));
	}

	public function create() {

		return view('admin_section.promocodes.create');
	}

	public function store(Request $request) {
		DB::beginTransaction();

		$promocode = new Promocode;
		$promocode->code = $request->code;
		$promocode->habilitado = 0;
		$promocode->save();

		if (!$promocode) {

			DB::rollBack();
			return redirect()->route('administrador.promocodes')->with('success', 'Error Creando Promocode');
		}
		DB::commit();
			return redirect()->route('administrador.promocodes')->with('success', 'Exito Creando Promocode');
	}

	public function edit($id) {
		try {
			$d = Crypt::decrypt($id);
		} catch (DecryptException $e) {
			abort(404);
		}
    	$promocode = Promocode::where('id', '=', $d)->first();
		return view('admin_section.promocodes.edit', compact('promocode'));

	}

	public function update(Request $request) {
		$promocode = Promocode::find($request->id);

				DB::beginTransaction();

				$promocode->code = $request->code;
				$promocode->save();

				if (!$promocode) {
					$id = Crypt::encrypt($promocode->id);
					DB::rollBack();
					return redirect()->route('administrador.promocodes')->with('success', 'Error Modificando Promocode');
				}
				DB::commit();
			return redirect()->route('administrador.promocodes')->with('success', 'Exito Modificando Promocode');
	}

    public function destroy($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $promocode = Promocode::findOrfail($d);
     	$promocode->delete();
			return redirect()->route('administrador.promocodes')->with('success', 'Exito Eliminando Promocode');
    }

    public function EstadoPromocode($id)
    {
        try {
            $promocode_id = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $promocode = Promocode::findOrfail($promocode_id);

        if ($promocode->habilitado == '0' || $promocode->habilitado == null) {
            $promocode->habilitado = '1';
            $promocode->save();
        }
        else{
            $promocode->habilitado = '0';
            $promocode->save();           
        }

        return redirect()->route('administrador.promocodes')->with('success', 'Promocode  Habilitado Exitosamente');

    }




}
