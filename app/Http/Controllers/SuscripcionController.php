<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\RegistroUsuario;
use App\User;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Illuminate\Auth\Events\Registered;
use DB;


class SuscripcionController extends Controller
{
    public function pago(Request $request)
    {

        try {
            Stripe::setApiKey(config('services.stripe.secret'));
        $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken
            ));
        $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => 2100,
                'currency' => 'usd'
            ));
        return 'Cargo exitoso!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

    }

    public function vista_suscripcion(Request $request)
    {
	    return view('suscripcion');
    }

	public function procesa_suscripcion(Request $request)
	{
	    try {

        DB::beginTransaction();
            Stripe::setApiKey(config('services.stripe.secret'));
            $user_sesion = $request->session()->get('C4xAcademy.user.data');

            $user = User::create([
                    'nombre'       => $user_sesion['nombre'],
                    'apellido'     => $user_sesion['apellido'],
                    'nickname'     => $user_sesion['nickname'],
                    'email'        => $user_sesion['email'],
                    'password'     => $user_sesion['password'],
                    'pais'         => $user_sesion['pais'],
                    'telefono'     => $user_sesion['telefono'],
                    'sexo'         => $user_sesion['sexo'],
                    'role'         => 'USUARIO',
                    'habilitado'   => 1,
                    'edad'         => $user_sesion['edad'],
                    'plataforma'   => $user_sesion['plataforma'],
                    'telegram'     => $user_sesion['telegram'],
            ]);
            $user->newSubscription('main', 'keylevel-alerts')->create($request->stripeToken);
            event(new Registered($user));
        DB::commit();
        if (!$user) {
            DB::rollBack();
            return redirect()->route('login')->with('success', 'Error Pagando el Servicio');
        }


            try {
                Mail::to($user->email)->send(new RegistroUsuario($user));
            } catch (\Exception $ex) {
                return redirect()->route('login')->with([
                    'success' => 'Suscripción exitosa! Acabas de suscribirte a Key Level Alerts de C4xAcademy, pero no se envió el correo.',
                ]);
            }

            return redirect()->route('login')->with([
                'success' => 'Suscripción exitosa! Acabas de suscribirte a Key Level Alerts de C4xAcademy',
            ]);

	    } catch (\Exception $ex) {
            return redirect()->route('register')->with('success', $ex->getMessage());
	    }
	}

}
