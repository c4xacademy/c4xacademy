<?php

namespace App\Http\Controllers;
use App\User;
use App\Categoria;
use App\Archivo;
use App\Video;
use DB;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Validator;

class UserPreviewController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */


    public function index_preview(Request $request) {
        $plataforma_sesion = $request->session()->get('plataforma.data');
//        $request->session()->forget('plataforma.data');

        $videos_total = Video::where('plataforma', '=', $plataforma_sesion)->count();

        $videos_vistos = DB::table('visto_notificacions')
            ->join('videos', 'visto_notificacions.video_id', '=', 'videos.video_id')
            ->join('users', 'visto_notificacions.user_id', '=', 'users.id')
            ->where('visto_notificacions.user_id', '=', Auth::user()->id)
            ->where('visto_notificacions.visto', '=', true)
            ->where('videos.plataforma', '=', $plataforma_sesion)
            ->where('users.plataforma', '=', $plataforma_sesion)
            ->select('videos.*', 'visto_notificacions.visto')
            ->count();

        if($videos_total == 0){ 
            $porcentaje = 0;            
        }
        else{
            $porcentaje = ($videos_vistos * 100)/($videos_total);        
        }
    	$categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        return view('user_section.preview.index', compact('categorias', 'porcentaje'));
    }



    public function perfil_preview(Request $request, $id)
    {
        $plataforma_sesion = $request->session()->get('plataforma.data');
        $categorias = Categoria::where('habilitada', '=', 1)->where('plataforma', '=', $plataforma_sesion)->get();
        try {
            $usuario_id = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $usuario = User::findOrfail($usuario_id)->first();

        return view('user_section.preview.perfil_usuario.perfil', compact('usuario', 'categorias'));
    }

    public function update_perfil(Request $request) {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();


        if ($user->count() != 0) {
            if ($user[0]->id != Auth::user()->id) {
                Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ])->validate();
            }
        }


                Validator::make($request->all(), [
                'nombre'   => 'max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                'apellido' => 'regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                'nickname' => 'alpha_num|max:191',
                'password' => 'confirmed',
                'pais'     => 'max:30|nullable|present',
                'telefono'      => 'max:15|nullable|present|regex:[\+?[0-9]{9,14}]',

                    ])->validate();


        $usuario = User::find(Auth::user()->id);

        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->pais     = $request->pais;
        $usuario->telefono = $request->telefono;
        $usuario->nickname = $request->nickname;

        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }

        $usuario->save();

        $mensaje = "User Updated";
        return redirect()->route('user_preview.perfil', ['id' => Crypt::encrypt($usuario->id)])->withInput()->with('success', $mensaje);

    }

}
