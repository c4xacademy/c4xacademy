<?php

namespace App\Http\Controllers;

use App\Mail\RegistroUsuario;
use App\Order;
use App\Promocode;
use App\Helpers\PayPal;
use App\Service;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use DB;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Illuminate\Auth\Events\Registered;

/**
 * Class PayPalController
 * @package App\Http\Controllers
 */
class PayPalController extends Controller
{
    
    
    public function form(Request $request, $service_id)
    {       

            return redirect()->route('register')->with([
                'success' => 'No se pudo completar el pago ni el registro.',
            ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function checkout(Request $request, $transaction_id)
    {
            $user_sesion = $request->session()->get('C4xAcademy.user.data');

            $order = new Order;
            $order->user_name = $user_sesion['nombre'] ;  //user id
            $order->transaction_id = $transaction_id;

            if($user_sesion['plataforma'] == 0){
                $order->service_id    = 1;                
                $service = Service::findOrFail(1);
            }
            if($user_sesion['plataforma'] == 1){
                $order->service_id    = 2;                
                $service = Service::findOrFail(2);
            }
            $order->amount  = $service->amount;
            $order->save();


            $paypal = new PayPal;

            $response = $paypal->purchase([
                'amount' => $paypal->formatAmount($order->amount),
                'transactionId' => $order->transaction_id,
                'currency' => 'USD',
                'cancelUrl' => $paypal->getCancelUrl($order),
                'returnUrl' => $paypal->getReturnUrl($order),
            ]);


            if ($response->isRedirect()) {
                $response->redirect();
            }

            return redirect()->back()->with([
                 'success' => $response->getMessage(),
                
            ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     * @return mixed
     */
    public function completed($order_id, Request $request)
    {

        $order = Order::findOrFail($order_id);

        $paypal = new PayPal;

        $response = $paypal->complete([
            'amount' => $paypal->formatAmount($order->amount),
            'transactionId' => $order->transaction_id,
            'currency' => 'USD',
            'cancelUrl' => $paypal->getCancelUrl($order),
            'returnUrl' => $paypal->getReturnUrl($order),
            'notifyUrl' => $paypal->getNotifyUrl($order),
        ]);

        if ($response->isSuccessful()) {
            $order->update([
                'transaction_id' => $response->getTransactionReference(),
                'payment_status' => Order::PAYMENT_COMPLETED,
            ]);

            return redirect()->route('paymentCompleted', encrypt($order_id))->with([
                'message' => 'You recent payment is sucessful with reference code ' . $response->getTransactionReference(),
            ]);
        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);

    }
    public function paymentCompleted(Request $request, $order)
    {
        $user_sesion = $request->session()->get('C4xAcademy.user.data');

            $user = User::create([
                    'nombre'       => $user_sesion['nombre'],
                    'apellido'     => $user_sesion['apellido'],
                    'nickname'     => $user_sesion['nickname'],
                    'email'        => $user_sesion['email'],
                    'password'     => $user_sesion['password'],
                    'pais'         => $user_sesion['pais'],
                    'telefono'     => $user_sesion['telefono'],
                    'sexo'         => $user_sesion['sexo'],
                    'role'         => 'USUARIO',
                    'habilitado'   => 1,
                    'edad'         => $user_sesion['edad'],
                    'plataforma'   => $user_sesion['plataforma'],
            ]);

            event(new Registered($user));

                $videos = Video::where('plataforma', '=', $request->plataforma)->get();
                foreach ($videos as $video) {
                    DB::insert('insert into visto_notificacions (user_id, video_id, notificado, visto) values(?,?,?,?)', [$user->id, $video->video_id, '1', '0']);
                }



            try {
                Mail::to($user->email)->send(new RegistroUsuario($user));
            } catch (\Exception $ex) {
                return redirect()->route('login')->with([
                    'success' => 'Pago realizado correctamente, usuario creado y habilitado. Puede acceder a la plataforma.',
                ]);
            }

            return redirect()->route('login')->with([
                'success' => 'Pago realizado correctamente, usuario creado y habilitado. Puede acceder a la plataforma.',
            ]);


    }

    /**
     * @param $order_id
     */
    public function cancelled($order_id)
    {
        $order = Order::findOrFail($order_id);

        return redirect()->route('order.paypal', encrypt($order_id))->with([
            'message' => 'You have cancelled your recent PayPal payment !',
        ]);
    }

        /**
     * @param $order_id
     * @param $env
     * @param Request $request
     */
    public function webhook($order_id, $env, Request $request)
    {
        // to do with new release of sudiptpa/paypal-ipn v3.0 (under development)
    }
    
}