<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        if (Auth::guard($guard)->check()) {
            $plataforma = Auth::user()->plataforma;

            if (Auth::user()->habilitado == '0') {
                Auth::guard()->logout();
                $request->session()->flush();

                $request->session()->regenerate();
                $mensaje = "Por favor espere a que su cuenta sea activada.";
                return redirect('/login')->withInput()->with('errors', $mensaje);
            }

            if (Auth::user()->role == "ADMINISTRADOR") {
                return redirect('administrador');
            }

            return redirect('/select');
        }

        return $next($request);
    }
}
