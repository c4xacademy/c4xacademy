<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">

<style>
    body{
      background: url('{{asset('imagenes/fondo.JPG')}}') no-repeat center center fixed;
      background-attachment: fixed;
      background-size: cover;
    }
    .card{
        width: 200px;
        height: auto;
        display: inline-block;
        color: #000000;
        background-color: rgba(255,255,255,.7);
        margin:20px;
        padding:0px 10px 0px 10px;

        cursor: pointer;

        opacity: .6;

        -webkit-border-radius: 5px 5px 3px 3px;
        -moz-border-radius: 5px 5px 3px 3px;
        border-radius: 5px 5px 3px 3px;

        -webkit-box-shadow: 0px 0px 5px -1px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 5px -1px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 5px -1px rgba(0,0,0,0.75);
        transition: all .7s ease;
    }

    .card h4{
      font-size: 12pt;
      font-weight: 600;
    }

    .card:hover{
      opacity: 1;

      -webkit-box-shadow: 0px 10px 20px -6px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 10px 20px -6px rgba(0,0,0,0.75);
      box-shadow: 0px 10px 20px -6px rgba(0,0,0,0.75);

      -webkit-box-shadow: 0px 12px 67px -3px rgba(0,100,0,1);
      -moz-box-shadow: 0px 12px 67px -3px rgba(0,100,0,1);
      box-shadow: 0px 12px 67px -3px rgba(0,100,0,1);

      background-color: rgba(011,100,110,.117);
      color:#fff;

      transition-duration:.7s ease-out;
    }
    .card1 img{
      border-bottom: 1px solid rgba(0,0,0,.3);
    }
    .brand{
      color: #006400;
      border:1px solid rgba(255,255,255,.7);
      margin-top:30px;
      background-color: rgba(255,255,255,.5);
      -webkit-box-shadow: 0px 0px 46px -3px rgba(0,100,0,1);
      -moz-box-shadow: 0px 0px 46px -3px rgba(0,100,0,1);
      box-shadow: 0px 0px 46px -3px rgba(0,100,0,1);
    }
    .brand .tittle{
      font-size: 50pt;
      padding-top: 25px;

      vertical-align: middle;
      line-height: 0.3;
    }
    .brand .description{
      font-size: 9pt;
      margin-top: 13px;
    }
    @media(max-width: 768px){

    }
    @media(min-width: 769px){
      .brand .tittle{
        line-height: 0.7;
        margin-bottom: 18px;
      }
    }
    .img-custom{
        width: 200px;
        height: 200px;
    }
</style>


<body class="container">
  <div class="row">

    {{--@include('layouts.partials.mainheader')--}}
<br>
<br>
<br>
    <div class=" col-md-offset-4 col-md-4 col-md-offset-4 visible-md visible-lg"  >
        <center><h1 style="font-size: 50px; font-style: italic; color: #ffffff">Pago de Suscripción con Stripe</h1></center>
    </div>
<br>
<br>
<br>

    <div class="clearfix"></div>

    <!-- Content Wrapper. Contains page content -->

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
<center>
<div>
    {!! Form::open(array('route' => 'suscripcion.stripe','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
        {{ csrf_field() }}
        <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="{{ config('services.stripe.key') }}"
            data-amount="1990"
            data-name="Compra"
            data-description="Suscripción Key Level Alerts"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto">
        </script>
{!! Form::close() !!}
</div>
</center>

    </section><!-- /.content -->
  </div>

<!-- jquery -->
<script type="text/javascript" src="{{asset('page_assets/js/jquery-1.12.4.min.js')}}"></script>

<!-- plugins-jquery -->
<script type="text/javascript" src="{{asset('page_assets/js/plugins-jquery.js')}}"></script>

</body>
</html>