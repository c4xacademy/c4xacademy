<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Átomo Virtual">
    <title>C4xAcademy | Panel de Login</title>

    <!-- Favicons-->
  <link rel="shortcut icon" href="{{URL::asset('page_assets/images/favicon.png')}}" />
  <link rel="apple-touch-icon" type="image/x-icon" href="{{URL::asset('auth_assets/img/apple-touch-icon-57x57-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{URL::asset('auth_assets/img/apple-touch-icon-72x72-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{URL::asset('auth_assets/img/img/apple-touch-icon-114x114-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{URL::asset('auth_assets/img/img/apple-touch-icon-114x114-precomposed.png')}}">

    <!-- BASE CSS -->
    <link href="{{asset('auth_assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('auth_assets/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/vendors.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/icon_fonts/css/all_icons.min.css')}}" rel="stylesheet">
	
	<!-- SPECIFIC CSS -->
	<link href="{{asset('auth_assets/css/skins/square/grey.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/wizard.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('auth_assets/css/custom.css')}}" rel="stylesheet">

</head>

<body id="login_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure>
				<a href="{{URL('/')}}"><img src="{{URL::asset('page_assets/images/logo.png')}}" width="149" height="82" data-retina="true" alt=""></a>
			</figure>
            <form method="POST" action="{{ route('login') }}">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="divider"><span>Iniciar Sesión</span></div>

            <br><strong></strong><br>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Hubo un problema al Iniciar Sesión<br><br>
                <ul>
                        <li>{{ $errors }}</li>
                </ul>
            </div>
        @endif


	    @if ($message = Session::get('success'))
            <br><strong></strong><br>
	        <div class="alert alert-success">
	            <p>{{ $message }}</p>
	        </div>
	    @endif
				<div class="form-group">
					<span class="input">
					<input class="input_field" type="email" autocomplete="off" name="email" required autofocus>
						<label class="input_label">
						<span class="input__label-content">E-mail: </span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password" autocomplete="new-password" name="password" required autofocus>
						<label class="input_label">
						<span class="input__label-content">Contraseña: </span>
					</label>
					</span>
					<small><a href="{{ route('password.request') }}">
                			¿Olvidó su contraseña?
                		   </a>
                	</small>
				</div>
                <button type="submit" class="btn_1 rounded full-width add_top_60">
                	{{ __('Iniciar Sesión') }}
                </button>
				<div class="text-center add_top_10">Nuevo en la Academia? <strong><a href="{{ route('register') }}">Registrarse!</a></strong></div>
			</form>
			<div class="copy">
             <p> C4xAcademy &copy; Se reserva Todos los Derechos de Autor.<br>
            Desarrollado con ♥ por: <a href="https://www.facebook.com/atomotecnologiasvirtuales/">Átomo Tecnologías Virtuales</a></p>
			</div>
		</aside>
	</div>
	<!-- /login -->
		
	<!-- COMMON SCRIPTS -->
    <script src="{{asset('auth_assets/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth_assets/js/common_scripts.js')}}"></script>
    <script src="{{asset('auth_assets/js/main.js')}}"></script>
	<script src="{{asset('auth_assets/assets/validate.js')}}"></script>
  
</body>
</html>