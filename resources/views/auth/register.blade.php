<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Átomo Virtual">
    <title>C4xAcademy | Panel de Registro</title>

    <!-- Favicons-->
  <link rel="shortcut icon" href="{{URL::asset('page_assets/images/favicon.png')}}" />
  <link rel="apple-touch-icon" type="image/x-icon" href="{{URL::asset('auth_assets/img/apple-touch-icon-57x57-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{URL::asset('auth_assets/img/apple-touch-icon-72x72-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{URL::asset('auth_assets/img/img/apple-touch-icon-114x114-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{URL::asset('auth_assets/img/img/apple-touch-icon-114x114-precomposed.png')}}">

    <!-- BASE CSS -->
    <link href="{{asset('auth_assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('auth_assets/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/vendors.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/icon_fonts/css/all_icons.min.css')}}" rel="stylesheet">
	
	<!-- SPECIFIC CSS -->
	<link href="{{asset('auth_assets/css/skins/square/grey.css')}}" rel="stylesheet">
	<link href="{{asset('auth_assets/css/wizard.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('auth_assets/css/custom.css')}}" rel="stylesheet">

</head>

<body id="admission_bg">
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="form_container" class="clearfix">
		<figure>
                <a href="{{URL('/')}}"><img src="{{URL::asset('page_assets/images/logo.png')}}" width="149" height="82" data-retina="true" alt=""></a>
		</figure>
		<div id="wizard_container">
			<div id="top-wizard">
				<div id="progressbar"></div>
			</div>
			<!-- /top-wizard -->
    {!! Form::open(array('route' => 'register','method'=>'POST', 'enctype' => 'multipart/form-data', 'id'=>'wrapped', 'name' => 'example-1')) !!}
    {{ csrf_field() }}
				<input id="website" name="website" type="text" value="">
				<!-- Leave for security protection, read docs for details -->
				<div id="middle-wizard">
					<div class="step">
						<div id="intro">
							<figure><img src="{{URL::asset('auth_assets/img/wizard_intro_icon.svg')}}" alt=""></figure>
					     	@if (count($errors) > 0)
					        <div class="alert alert-danger">
					            <br><br>
					            <ul>
					                @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					        @endif

						    @if ($message = Session::get('success'))
						        <div class="alert alert-danger">
						            <p>{{ $message }}</p>
						        </div>
						    @endif
							<h1>Formulario de Registro</h1>
							<p>Bienvenidos a nuestro formulario de registro, para obtener completo acceso a nuestro curso complete el siguiente formulario. </p>
							<p><strong>Es importante que para que el registro sea exitoso, complete cada uno de los campos de registro.</strong></p>
						</div>
					</div>

					<div class="step">
						<h3 class="main_question"><strong>1/3</strong>Por favor llene todos los campos.</h3>
						<div class="form-group">
							<input type="text" name="nombre" class="form-control required" placeholder="Nombres: ">
					       	@if ($errors->has('nombre'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('nombre') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
							<input type="text" name="apellido" class="form-control required" placeholder="Apellidos: ">
					       	@if ($errors->has('apellido'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('apellido') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
							<input type="text" name="telefono" class="form-control" placeholder="Teléfono: ">
					       	@if ($errors->has('telefono'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('telefono') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
							<input type="text" name="edad" class="form-control" placeholder="Edad: ">
					       	@if ($errors->has('edad'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('edad') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group select">
							<div class="styled-select">
								<select class="required" name="pais" id="pais">
									<option value="" selected="">Seleccione un País</option>
                                    <option value="Afganistán" id="AF">Afganistán</option>
                                    <option value="Albania" id="AL">Albania</option>
                                    <option value="Alemania" id="DE">Alemania</option>
                                    <option value="Andorra" id="AD">Andorra</option>
                                    <option value="Angola" id="AO">Angola</option>
                                    <option value="Anguila" id="AI">Anguila</option>
                                    <option value="Antártida" id="AQ">Antártida</option>
                                    <option value="Antigua y Barbuda" id="AG">Antigua y Barbuda</option>
                                    <option value="Antillas holandesas" id="AN">Antillas holandesas</option>
                                    <option value="Arabia Saudí" id="SA">Arabia Saudí</option>
                                    <option value="Argelia" id="DZ">Argelia</option>
                                    <option value="Argentina" id="AR">Argentina</option>
                                    <option value="Armenia" id="AM">Armenia</option>
                                    <option value="Aruba" id="AW">Aruba</option>
                                    <option value="Australia" id="AU">Australia</option>
                                    <option value="Austria" id="AT">Austria</option>
                                    <option value="Azerbaiyán" id="AZ">Azerbaiyán</option>
                                    <option value="Bahamas" id="BS">Bahamas</option>
                                    <option value="Bahrein" id="BH">Bahrein</option>
                                    <option value="Bangladesh" id="BD">Bangladesh</option>
                                    <option value="Barbados" id="BB">Barbados</option>
                                    <option value="Bélgica" id="BE">Bélgica</option>
                                    <option value="Belice" id="BZ">Belice</option>
                                    <option value="Benín" id="BJ">Benín</option>
                                    <option value="Bermudas" id="BM">Bermudas</option>
                                    <option value="Bhután" id="BT">Bhután</option>
                                    <option value="Bielorrusia" id="BY">Bielorrusia</option>
                                    <option value="Birmania" id="MM">Birmania</option>
                                    <option value="Bolivia" id="BO">Bolivia</option>
                                    <option value="Bosnia y Herzegovina" id="BA">Bosnia y Herzegovina</option>
                                    <option value="Botsuana" id="BW">Botsuana</option>
                                    <option value="Brasil" id="BR">Brasil</option>
                                    <option value="Brunei" id="BN">Brunei</option>
                                    <option value="Bulgaria" id="BG">Bulgaria</option>
                                    <option value="Burkina Faso" id="BF">Burkina Faso</option>
                                    <option value="Burundi" id="BI">Burundi</option>
                                    <option value="Cabo Verde" id="CV">Cabo Verde</option>
                                    <option value="Camboya" id="KH">Camboya</option>
                                    <option value="Camerún" id="CM">Camerún</option>
                                    <option value="Canadá" id="CA">Canadá</option>
                                    <option value="Chad" id="TD">Chad</option>
                                    <option value="Chile" id="CL">Chile</option>
                                    <option value="China" id="CN">China</option>
                                    <option value="Chipre" id="CY">Chipre</option>
                                    <option value="Ciudad estado del Vaticano" id="VA">Ciudad estado del Vaticano</option>
                                    <option value="Colombia" id="CO">Colombia</option>
                                    <option value="Comores" id="KM">Comores</option>
                                    <option value="Congo" id="CG">Congo</option>
                                    <option value="Corea" id="KR">Corea</option>
                                    <option value="Corea del Norte" id="KP">Corea del Norte</option>
                                    <option value="Costa del Marfíl" id="CI">Costa del Marfíl</option>
                                    <option value="Costa Rica" id="CR">Costa Rica</option>
                                    <option value="Croacia" id="HR">Croacia</option>
                                    <option value="Cuba" id="CU">Cuba</option>
                                    <option value="Dinamarca" id="DK">Dinamarca</option>
                                    <option value="Djibouri" id="DJ">Djibouri</option>
                                    <option value="Dominica" id="DM">Dominica</option>
                                    <option value="Ecuador" id="EC">Ecuador</option>
                                    <option value="Egipto" id="EG">Egipto</option>
                                    <option value="El Salvador" id="SV">El Salvador</option>
                                    <option value="Emiratos Arabes Unidos" id="AE">Emiratos Arabes Unidos</option>
                                    <option value="Eritrea" id="ER">Eritrea</option>
                                    <option value="Eslovaquia" id="SK">Eslovaquia</option>
                                    <option value="Eslovenia" id="SI">Eslovenia</option>
                                    <option value="España" id="ES">España</option>
                                    <option value="Estados Unidos" id="US">Estados Unidos</option>
                                    <option value="Estonia" id="EE">Estonia</option>
                                    <option value="c" id="ET">Etiopía</option>
                                    <option value="Ex-República Yugoslava de Macedonia" id="MK">Ex-República Yugoslava de Macedonia</option>
                                    <option value="Filipinas" id="PH">Filipinas</option>
                                    <option value="Finlandia" id="FI">Finlandia</option>
                                    <option value="Francia" id="FR">Francia</option>
                                    <option value="Gabón" id="GA">Gabón</option>
                                    <option value="Gambia" id="GM">Gambia</option>
                                    <option value="Georgia" id="GE">Georgia</option>
                                    <option value="Georgia del Sur y las islas Sandwich del Sur" id="GS">Georgia del Sur y las islas Sandwich del Sur</option>
                                    <option value="Ghana" id="GH">Ghana</option>
                                    <option value="Gibraltar" id="GI">Gibraltar</option>
                                    <option value="Granada" id="GD">Granada</option>
                                    <option value="Grecia" id="GR">Grecia</option>
                                    <option value="Groenlandia" id="GL">Groenlandia</option>
                                    <option value="Guadalupe" id="GP">Guadalupe</option>
                                    <option value="Guam" id="GU">Guam</option>
                                    <option value="Guatemala" id="GT">Guatemala</option>
                                    <option value="Guayana" id="GY">Guayana</option>
                                    <option value="Guayana francesa" id="GF">Guayana francesa</option>
                                    <option value="Guinea" id="GN">Guinea</option>
                                    <option value="Guinea Ecuatorial" id="GQ">Guinea Ecuatorial</option>
                                    <option value="Guinea-Bissau" id="GW">Guinea-Bissau</option>
                                    <option value="Haití" id="HT">Haití</option>
                                    <option value="Holanda" id="NL">Holanda</option>
                                    <option value="Honduras" id="HN">Honduras</option>
                                    <option value="Hong Kong R. A. E" id="HK">Hong Kong R. A. E</option>
                                    <option value="Hungría" id="HU">Hungría</option>
                                    <option value="India" id="IN">India</option>
                                    <option value="Indonesia" id="ID">Indonesia</option>
                                    <option value="Irak" id="IQ">Irak</option>
                                    <option value="Irán" id="IR">Irán</option>
                                    <option value="Irlanda" id="IE">Irlanda</option>
                                    <option value="Isla Bouvet" id="BV">Isla Bouvet</option>
                                    <option value="Isla Christmas" id="CX">Isla Christmas</option>
                                    <option value="Isla Heard e Islas McDonald" id="HM">Isla Heard e Islas McDonald</option>
                                    <option value="Islandia" id="IS">Islandia</option>
                                    <option value="Islas Caimán" id="KY">Islas Caimán</option>
                                    <option value="Islas Cook" id="CK">Islas Cook</option>
                                    <option value="Islas de Cocos o Keeling" id="CC">Islas de Cocos o Keeling</option>
                                    <option value="Islas Faroe" id="FO">Islas Faroe</option>
                                    <option value="Islas Fiyi" id="FJ">Islas Fiyi</option>
                                    <option value="Islas Malvinas Islas Falkland" id="FK">Islas Malvinas Islas Falkland</option>
                                    <option value="Islas Marianas del norte" id="MP">Islas Marianas del norte</option>
                                    <option value="Islas Marshall" id="MH">Islas Marshall</option>
                                    <option value="Islas menores de Estados Unidos" id="UM">Islas menores de Estados Unidos</option>
                                    <option value="Islas Palau" id="PW">Islas Palau</option>
                                    <option value="Islas Salomón" d="SB">Islas Salomón</option>
                                    <option value="Islas Tokelau" id="TK">Islas Tokelau</option>
                                    <option value="Islas Turks y Caicos" id="TC">Islas Turks y Caicos</option>
                                    <option value="Islas Vírgenes EE.UU." id="VI">Islas Vírgenes EE.UU.</option>
                                    <option value="Islas Vírgenes Reino Unido" id="VG">Islas Vírgenes Reino Unido</option>
                                    <option value="Israel" id="IL">Israel</option>
                                    <option value="Italia" id="IT">Italia</option>
                                    <option value="Jamaica" id="JM">Jamaica</option>
                                    <option value="Japón" id="JP">Japón</option>
                                    <option value="Jordania" id="JO">Jordania</option>
                                    <option value="Kazajistán" id="KZ">Kazajistán</option>
                                    <option value="Kenia" id="KE">Kenia</option>
                                    <option value="Kirguizistán" id="KG">Kirguizistán</option>
                                    <option value="Kiribati" id="KI">Kiribati</option>
                                    <option value="Kuwait" id="KW">Kuwait</option>
                                    <option value="Laos" id="LA">Laos</option>
                                    <option value="Lesoto" id="LS">Lesoto</option>
                                    <option value="Letonia" id="LV">Letonia</option>
                                    <option value="Líbano" id="LB">Líbano</option>
                                    <option value="Liberia" id="LR">Liberia</option>
                                    <option value="Libia" id="LY">Libia</option>
                                    <option value="Liechtenstein" id="LI">Liechtenstein</option>
                                    <option value="Lituania" id="LT">Lituania</option>
                                    <option value="Luxemburgo" id="LU">Luxemburgo</option>
                                    <option value="Macao R. A. E" id="MO">Macao R. A. E</option>
                                    <option value="Madagascar" id="MG">Madagascar</option>
                                    <option value="Malasia" id="MY">Malasia</option>
                                    <option value="Malawi" id="MW">Malawi</option>
                                    <option value="Maldivas" id="MV">Maldivas</option>
                                    <option value="Malí" id="ML">Malí</option>
                                    <option value="Malta" id="MT">Malta</option>
                                    <option value="Marruecos" id="MA">Marruecos</option>
                                    <option value="Martinica" id="MQ">Martinica</option>
                                    <option value="Mauricio" id="MU">Mauricio</option>
                                    <option value="Mauritania" id="MR">Mauritania</option>
                                    <option value="Mayotte" id="YT">Mayotte</option>
                                    <option value="México" id="MX">México</option>
                                    <option value="Micronesia" id="FM">Micronesia</option>
                                    <option value="Moldavia" id="MD">Moldavia</option>
                                    <option value="Mónaco" id="MC">Mónaco</option>
                                    <option value="Mongolia" id="MN">Mongolia</option>
                                    <option value="Montserrat" id="MS">Montserrat</option>
                                    <option value="Mozambique" id="MZ">Mozambique</option>
                                    <option value="Namibia" id="NA">Namibia</option>
                                    <option value="Nauru" id="NR">Nauru</option>
                                    <option value="Nepal" id="NP">Nepal</option>
                                    <option value="Nicaragua" id="NI">Nicaragua</option>
                                    <option value="Níger" id="NE">Níger</option>
                                    <option value="Nigeria" id="NG">Nigeria</option>
                                    <option value="Niue" id="NU">Niue</option>
                                    <option value="Norfolk" id="NF">Norfolk</option>
                                    <option value="Noruega" id="NO">Noruega</option>
                                    <option value="Nueva Caledonia" id="NC">Nueva Caledonia</option>
                                    <option value="Nueva Zelanda" id="NZ">Nueva Zelanda</option>
                                    <option value="Omán" id="OM">Omán</option>
                                    <option value="Panamá" id="PA">Panamá</option>
                                    <option value="Papua Nueva Guinea" id="PG">Papua Nueva Guinea</option>
                                    <option value="Paquistán" id="PK">Paquistán</option>
                                    <option value="Paraguay" id="PY">Paraguay</option>
                                    <option value="Perú" id="PE">Perú</option>
                                    <option value="Pitcairn" id="PN">Pitcairn</option>
                                    <option value="Polinesia francesa" id="PF">Polinesia francesa</option>
                                    <option value="Polonia" id="PL">Polonia</option>
                                    <option value="Portugal" id="PT">Portugal</option>
                                    <option value="Puerto Rico" id="PR">Puerto Rico</option>
                                    <option value="Qatar" id="QA">Qatar</option>
                                    <option value="Reino Unido" id="UK">Reino Unido</option>
                                    <option value="República Centroafricana" id="CF">República Centroafricana</option>
                                    <option value="República Checa" id="CZ">República Checa</option>
                                    <option value="República de Sudáfrica" id="ZA">República de Sudáfrica</option>
                                    <option value="República Democrática del Congo Zaire" id="CD">República Democrática del Congo Zaire</option>
                                    <option value="República Dominicana" id="DO">República Dominicana</option>
                                    <option value="Reunión" id="RE">Reunión</option>
                                    <option value="Ruanda" id="RW">Ruanda</option>
                                    <option value="Rumania" id="RO">Rumania</option>
                                    <option value="Rusia" id="RU">Rusia</option>
                                    <option value="Samoa" id="WS">Samoa</option>
                                    <option value="Samoa occidental" id="AS">Samoa occidental</option>
                                    <option value="San Kitts y Nevis" id="KN">San Kitts y Nevis</option>
                                    <option value="San Marino" id="SM">San Marino</option>
                                    <option value="San Pierre y Miquelon" id="PM">San Pierre y Miquelon</option>
                                    <option value="San Vicente e Islas Granadinas" id="VC">San Vicente e Islas Granadinas</option>
                                    <option value="Santa Helena" id="SH">Santa Helena</option>
                                    <option value="Santa Lucía" id="LC">Santa Lucía</option>
                                    <option value="Santo Tomé y Príncipe" id="ST">Santo Tomé y Príncipe</option>
                                    <option value="Senegal" id="SN">Senegal</option>
                                    <option value="Serbia y Montenegro" id="YU">Serbia y Montenegro</option>
                                    <option value="Sychelles" id="SC">Seychelles</option>
                                    <option value="Sierra Leona" id="SL">Sierra Leona</option>
                                    <option value="Singapur" id="SG">Singapur</option>
                                    <option value="Siria" id="SY">Siria</option>
                                    <option value="Somalia" id="SO">Somalia</option>
                                    <option value="Sri Lanka" id="LK">Sri Lanka</option>
                                    <option value="Suazilandia" id="SZ">Suazilandia</option>
                                    <option value="Sudán" id="SD">Sudán</option>
                                    <option value="Suecia" id="SE">Suecia</option>
                                    <option value="Suiza" id="CH">Suiza</option>
                                    <option value="Surinam" id="SR">Surinam</option>
                                    <option value="Svalbard" id="SJ">Svalbard</option>
                                    <option value="Tailandia" id="TH">Tailandia</option>
                                    <option value="Taiwán" id="TW">Taiwán</option>
                                    <option value="Tanzania" id="TZ">Tanzania</option>
                                    <option value="Tayikistán" id="TJ">Tayikistán</option>
                                    <option value="Territorios británicos del océano Indico" id="IO">Territorios británicos del océano Indico</option>
                                    <option value="Territorios franceses del sur" id="TF">Territorios franceses del sur</option>
                                    <option value="Timor Oriental" id="TP">Timor Oriental</option>
                                    <option value="Togo" id="TG">Togo</option>
                                    <option value="Tonga" id="TO">Tonga</option>
                                    <option value="Trinidad y Tobago" id="TT">Trinidad y Tobago</option>
                                    <option value="Túnez" id="TN">Túnez</option>
                                    <option value="Turkmenistán" id="TM">Turkmenistán</option>
                                    <option value="Turquía" id="TR">Turquía</option>
                                    <option value="Tuvalu" id="TV">Tuvalu</option>
                                    <option value="Ucrania" id="UA">Ucrania</option>
                                    <option value="Uganda" id="UG">Uganda</option>
                                    <option value="Uruguay" id="UY">Uruguay</option>
                                    <option value="Uzbekistán" id="UZ">Uzbekistán</option>
                                    <option value="Vanuatu" id="VU">Vanuatu</option>
                                    <option value="Venezuela" id="VE">Venezuela</option>
                                    <option value="Vietnam" id="VN">Vietnam</option>
                                    <option value="Wallis y Futuna" id="WF">Wallis y Futuna</option>
                                    <option value="Yemen" id="YE">Yemen</option>
                                    <option value="Zambia" id="ZM">Zambia</option>
                                    <option value="Zimbabue" id="ZW">Zimbabue</option>
								</select>
							</div>
						</div>
						<div class="form-group radio_input">
							<label><input type="radio" value="M" checked name="sexo" class="icheck">Masculino</label>
							<label><input type="radio" value="F" name="sexo" class="icheck">Femenino</label>
						</div>
					</div>
					<!-- /step-->

					<div class="step">
						<h3 class="main_question"><strong>2/3</strong>Por favor llene todos los campos.</h3>
						<div class="form-group">
						    <input id="nickname" type="text" class="form-control" name="nickname" placeholder="Nickname: " required autofocus>
					       	@if ($errors->has('nickname'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('nickname') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
							<input type="email" name="email" class="form-control required" placeholder="E-mail: ">
					       	@if ($errors->has('email'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('email') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
					        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña: " required autofocus>
					       	@if ($errors->has('password'))
				               	<span class="help-block">
				                   	<strong>{{ $errors->first('password') }}</strong>
				               	</span>
				           	@endif
						</div>
						<div class="form-group">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña: " required autofocus>
						</div>
					</div>
					<!-- /step-->

					<div class="submit step">
						<h3 class="main_question"><strong>3/3</strong>Por favor llene todos los campos.</h3>


                          <select onChange="pagoOnChangePlataforma(this)" id="plataforma" name="plataforma" class="form-control">
                                <option value="">Seleccionar Plataforma</option>
                                <option value="0">C4xAcademy (Español)</option>
<!--
                                <option value="1">C4xAcademy (Inglés/English)</option>
                                <option value="2">Key Level Alerts</option>
                                <option value="3">C4xAcademy (Preview)</option>
-->
                            @if ($errors->has('plataforma'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('plataforma') }}</strong>
                                </span>
                            @endif
                          </select>


                          <select onChange="pagoOnChange(this)" id="pago" name="pago" class="form-control">
                                <option value="">Seleccionar tipo de pago</option>
                                <option value="TRANSFERENCIA">Zelle</option>
                                <option value="BITCOINS">Bitcoin</option>
                                <option value="PAYPAL">PayPal</option>
                            @if ($errors->has('pago'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pago') }}</strong>
                                </span>
                            @endif
                          </select>
                        <div class="form-group" id="comprobante" style="display:none">
                            <p>El correo para el pago vía paypal de manera temporal es: Mceballos@ucol.mx</p>
                            <input placeholder="comprobante" data-placement="top" data-toggle="tooltip" data-original-title="El correo para el pago vía paypal de manera temporal es: Mceballos@ucol.mx" for="comprobante" id="comprobante" type="file" class="form-control" name="comprobante">                            
                        </div>
						<div class="form-group" id="conf" style="display:none">
                            <p>El correo para el pago vía zelle es: Mceballos@ucol.mx</p>
                            <input placeholder="Número de Confirmación" data-placement="top" data-toggle="tooltip" data-original-title="Si realiza el pago por transferencia, el número de confirmación es la referencia de la transferencia. Si el pago es por bitcoin, el número de confirmación es el wallet del que se realizó el pago. El pago lo pueden realizar via Zelle al correo: Mceballos@ucol.mx" for="confirmacion" id="confirmacion" type="text" class="form-control" name="confirmacion" value="">
                                @if ($errors->has('confirmacion'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('confirmacion') }}</strong>
                                    </span>
                                @endif
						</div>
                        <div class="form-group" id="telegram" style="display:none">
                            <input placeholder="Usuario de Telegram" data-placement="top" data-toggle="tooltip" data-original-title="Le recordamos que para recibir las alertas a través de nuestro servicio tiene que tener una cuenta de telegram, de esta manera las alertas serán más frecuentes y de contenido audiovisual." for="telegram" id="telegram" type="text" class="form-control" name="telegram" value="">
                                @if ($errors->has('telegram'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telegram') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group" id="promocode" style="display:none">
                            <input placeholder="Promocode " data-placement="top" data-toggle="tooltip" data-original-title="Le recordamos que al introducir uno de nuestros Promocodes se le realizará un descuento del 10% en su pago." for="promocode" id="promocode" type="text" class="form-control" name="promocode" value="">
                                @if ($errors->has('promocode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('promocode') }}</strong>
                                    </span>
                                @endif
                        </div>



						<div class="form-group terms">
							<input name="terminos" type="checkbox" class="icheck required" value="0">
							<label>Por favor acepte los <a href="#" data-toggle="modal" data-target="#terms-txt">Términos y Condiciones</a></label>
						</div>
					</div>
					<!-- /step-->
				</div>
				<!-- /middle-wizard -->
				<div id="bottom-wizard">
					<button type="button" name="backward" class="backward">Anterior </button>
					<button type="button" name="forward" class="forward">Siguiente</button>
					<button type="submit" name="process" class="submit">Enviar</button>
				</div>
				<!-- /bottom-wizard -->
			</form>
		</div>
		<!-- /Wizard container -->
	</div>
	<!-- /Form_container -->

	<!-- Modal terms -->
	<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="termsLabel">Términos y Condiciones</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
<p align="justify">Debido a que nuestros servicios y educación son un servicio digital y en vivo que se puede copiar y guardar fácilmente, no permitimos ni proporcionamos ningún reembolso por ningún motivo. Usted acepta esta política y acepta que no realizaremos ningún reembolso, sin importar cuál sea la condición.

Cuando compra nuestro servicio educativo, acepta esta política y respetará la perspectiva de <strong>www.C4xacademy.com</strong> (C4X academy LLC) sobre estos temas para obtener reembolsos. No garantizamos la exactitud, integridad o puntualidad de ninguna discusión o educación intercambiada a través de nuestro sitio o servicios de chat, y la información debe ser confirmada por otras fuentes. Todas las proyecciones son solo estimaciones. El material en el sitio o sala de chat no incluye: Una oferta o invitación de <strong>C4xacademy.com</strong>; o una recomendación de valores de C4X academy LLC o cualquier empleado, propietario o contratista independiente de C4X academy LLC para comprar o vender valores o cualquier otro producto financiero, moneda o producto básico.

Nada en este sitio o salas de chat constituye o sugiere asesoramiento financiero personal. El material, la discusión y la educación en este sitio o sala de chat no tienen en cuenta las necesidades personales de ninguna persona. Antes de tomar decisiones de inversión, debe contactar a un asesor financiero con licencia profesional. A excepción de la responsabilidad legal que no puede excluirse, C4X academy LLC y cualquier empleado, propietario o contratista independiente excluye toda responsabilidad (incluida la responsabilidad por negligencia) que surja del uso de cualquier servicio en este sitio o salas de chat. La responsabilidad que legalmente no puede excluirse se limita al máximo posible.

Este sitio no debe usarse como un sustituto del asesoramiento profesional bajo ninguna circunstancia. A excepción de la responsabilidad que legalmente no puede excluirse, C4X academy LLC y cualquier empleado, propietario o contratista independiente de C4X academy LLC, excluye toda responsabilidad (incluida la responsabilidad por negligencia) que surja del uso de datos, información, discusión, contribución, u otro, proporcionado por proveedores externos al sitio o sala de chat. La responsabilidad que legalmente no puede excluirse se limita al máximo posible. La educación con consejos generales o cualquier consejo o información en este sitio web es solo un consejo general, no considera sus circunstancias personales. Cualquier consejo general dado no debe tomarse como consejo profesional bajo ninguna circunstancia. Cualquier consejo general no debe tomarse como asesoramiento financiero específico bajo ninguna circunstancia. Cualquier consejo general no representa el deseo o la intención de C4X academy LLC de proporcionarle asesoría u orientación financiera.

No intercambie ni invierta solo en base a la información provista dentro del sitio http://C4xAcademy.com, o los chats asociados de C4X academy LLC. Al ver la información dentro del sitio web y las salas de chat en <strong>C4xacademy.com</strong> y las salas de chat asociadas, usted acepta que se trata de educación general y discusión general, y no hará responsable a C4X academy LLC ni a ninguna persona afiliada a C4X academy LLC de CUALQUIER pérdida o daño resultante de la información o el asesoramiento general proporcionado aquí por C4X academy LLC o sus empleados, propietarios o contratistas independientes. Las operaciones de cambio tienen grandes recompensas potenciales, pero también tienen factores de riesgo grandes y extremos. Debe ser consciente de los riesgos y estar dispuesto a aceptarlos para invertir en el mercado de divisas. Al firmar este descargo de responsabilidad, usted acepta tener una comprensión firme de los riesgos asociados con el mercado de divisas.

No negocies con dinero que no puedas permitirte perder. Este sitio web y las salas de chat asociadas no son una solicitud ni una oferta para comprar / vender divisas spot o cualquier otro producto financiero, moneda o producto básico. No se está haciendo ninguna representación de que cualquier tipo de cuenta, ya sea una cuenta real o una cuenta demo, logre ganancias o pérdidas como las discutidas en cualquier material en el sitio web, discusión de la sala de chat pasada, presente o futura o en cualquier fuente asociada de redes sociales. Los resultados anteriores y el rendimiento de cualquier sistema de negociación, comentario de mercado, discusión de chat, interacción con redes sociales u otra fuente, NO son garantía de resultados futuros y deben ser utilizados bajo su propio riesgo. Advertencia de alto riesgo: el alto grado de apalancamiento en las operaciones de cambio implica un riesgo extremo de pérdida y no es adecuado para todos los inversores. Al firmar este descargo de responsabilidad, usted comprende que su apalancamiento utilizado en los mercados de divisas se correlaciona directamente con su riesgo, y comprende y acepta que las cuentas de alto apalancamiento arrojan un mayor riesgo. Al firmar este descargo de responsabilidad, reconoce que C4X academy LLC no es responsable ni tiene influencia en ninguna decisión tomada con respecto al apalancamiento O cualquier otra decisión relacionada con la moneda.


Recuerde que los resultados anteriores de cualquier sistema de comercio o información no están garantizados de los resultados futuros de cualquier cuenta en vivo o demo. Al comprar nuestros servicios, usted acepta indemnizar y mantener a las subsidiarias y afiliadas de C4X academy LLC, y a cada uno de sus directores, funcionarios, agentes, contratistas, socios y empleados, u otros inofensivos y no culpables en el tribunal de justicia de y contra cualquier pérdida, responsabilidad, reclamo, demanda, daños, costos y gastos, incluidos los honorarios de abogados. Al firmar este descargo de responsabilidad, usted comprende y acepta que C4X academy LLC NO es un grupo "proveedor de señales", y NO ofrece NINGUNA asesoría u orientación financiera. Al firmar esta cláusula de exención de responsabilidad, usted comprende y reconoce su comprensión de que DEBE consultar a un asesor de asesoría financiera con licencia antes de tomar decisiones financieras o de inversión educadas.

Ganancias y resultados. El descargo de responsabilidad en cuanto a los resultados comerciales pasados o las ganancias pasadas en una cuenta real o cuenta demo no deben tomarse como una garantía, promesa o sugerencia de que puede lograr los mismos resultados en el futuro. La evidencia escrita o audiovisual de ganancias anteriores de una cuenta real o cuenta demo no es una garantía o promesa de que una persona o entidad alcanzará los mismos resultados o el mismo rendimiento cuando coloque operaciones en una cuenta real o en una cuenta demo. Al firmar esta cláusula de exención de responsabilidad, también reconoce que C4X academy LLC no pretende representar una cuenta real o una cuenta demo en ninguna circunstancia específica.

Las estrategias de negociación e información discutidas en el sitio web, salas de chat u otras fuentes, se han utilizado para operar en condiciones de mercado en vivo y demostración, pero esto no garantiza ni promete ganancias o pérdidas futuras. Si utiliza las estrategias / estrategias discutidas en cualquier plataforma de C4X academy LLC para realizar operaciones o decisiones de inversión, lo hace bajo su propio riesgo. C4X academy LLC y sus empleados, propietarios, contratistas independientes y otros, han utilizado en el pasado las estrategias de negociación discutidas en el sitio web, la sala de chat u otras fuentes para lograr tanto operaciones ganadoras como perdedoras.

Al utilizar <strong>C4xacademy.com</strong>, sus salas de chat o cualquier otra fuente, y las estrategias que se enseñan, muestran o analizan en su interior, se basan en su propia decisión y son su responsabilidad por completo. C4X academy LLC no es responsable en ningún caso de las pérdidas o daños causados por sus decisiones personales, acciones o intercambios, debe ponerse en contacto con un asesor profesional de inversiones antes de invertir en el mercado forex o en cualquier otro mercado financiero. En ningún momento se ha garantizado o prometido que las ganancias o los ingresos se obtendrán siguiendo / aplicando la educación, la discusión o la exhibición de divisas que tenga lugar en  <strong>C4xacademy.com</strong> o en cualquier sala de chat asociada u otra fuente. No se garantiza que se produzcan resultados similares en una cuenta real con dinero real o cuenta demo con dinero falso.

La interpretación juega un papel en los métodos / estrategias comerciales, entonces lo que usted cree que es una oportunidad comercial puede ser diferente a la visión de otra persona de una oportunidad comercial, tenga en cuenta que una estrategia comercial sin reglas mecánicas es interpretable, y C4X academy LLC</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn_1" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<!-- COMMON SCRIPTS -->
    <script src="{{asset('auth_assets/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth_assets/js/common_scripts.js')}}"></script>
    <script src="{{asset('auth_assets/js/main.js')}}"></script>
	<script src="{{asset('auth_assets/assets/validate.js')}}"></script>
	
	<!-- SPECIFIC SCRIPTS -->
	<script src="{{asset('auth_assets/js/jquery-ui-1.8.22.min.js')}}"></script>
	<script src="{{asset('auth_assets/js/jquery.wizard.js')}}"></script>
	<script src="{{asset('auth_assets/js/jquery.validate.js')}}"></script>
	<script src="{{asset('auth_assets/js/admission_func.js')}}"></script>
  

	<script type="text/javascript">
		function pagoOnChange(sel){
		      if (sel.value=="PAYPAL"){
		           divT = document.getElementById("conf");
		           divT.style.display = "none";
                   divT = document.getElementById("comprobante");
                   divT.style.display = "";

		      }
		      if (sel.value=="BITCOINS"){
		           divT = document.getElementById("conf");
		           divT.style.display = "none";
		           divT = document.getElementById("conf");
		           divT.style.display = "";
                   divT = document.getElementById("comprobante");
                   divT.style.display = "none";
		      }
		      if (sel.value=="TRANSFERENCIA"){
		           divT = document.getElementById("conf");
		           divT.style.display = "none";
		           divT = document.getElementById("conf");
		           divT.style.display = "";
                   divT = document.getElementById("comprobante");
                   divT.style.display = "none";
		      }
		      if (sel.value==""){
		           divT = document.getElementById("conf");
		           divT.style.display = "none";
		           divT = document.getElementById("conf");
		           divT.style.display = "none";
                   divT = document.getElementById("comprobante");
                   divT.style.display = "none";
		      }
		} 		


        function pagoOnChangePlataforma(sel){
              if (sel.value=="3"){ //C4xAcademy Preview
                   divT = document.getElementById("pago");
                   divT.style.display = "none";
                   divT = document.getElementById("conf");
                   divT.style.display = "none";
                   divT = document.getElementById("telegram");
                   divT.style.display = "none";
                   divT = document.getElementById("promocode");
                   divT.style.display = "none";
              }
              else{
                   divT = document.getElementById("pago");
                   divT.style.display = "";
                   divT = document.getElementById("telegram");
                   divT.style.display = "none";
                   divT = document.getElementById("promocode");
                   divT.style.display = "";
              }
              if (sel.value=="2"){ //C4xAcademy Key Level Alerts
                   divT = document.getElementById("pago");
                   divT.style.display = "none";
                   divT = document.getElementById("telegram");
                   divT.style.display = "";
                   divT = document.getElementById("promocode");
                   divT.style.display = "none";
              }
              if (sel.value==""){ //C4xAcademy Key Level Alerts
                   divT = document.getElementById("promocode");
                   divT.style.display = "none";
              }

        } 



	</script>



</body>
</html>