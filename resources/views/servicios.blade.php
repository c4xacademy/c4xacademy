<!DOCTYPE html>
<html lang="en">
@include('layouts.htmlheader2')

<body>

<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="{{URL::asset('page_assets/images/pre-loader/loader-02.svg')}}" alt="">
</div>

<!--=================================
 preloader -->

@include('layouts.header')

<!--=================================
 Secciones -->

<section class="rev-slider">
  <div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-6" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
  <div id="rev_slider_16_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.2">
    <ul>  <!-- SLIDE  -->
    <li data-index="rs-33" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{URL::asset($sliders[0]->sliders_ruta_imagen)}}"  data-delay="5990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
      <img src="{{URL::asset($sliders[0]->sliders_ruta_imagen)}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
      <!-- LAYER NR. 4 -->
      <div class="tp-caption   tp-resizeme"
        id="slide-34-layer-1"
        data-x="center" data-hoffset="200"
        data-y="center" data-voffset="10"
        data-width="['auto']"
        data-height="['auto']"
        
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":900,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['right','right','right','right']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
      style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 100px; font-weight: 400; color: #0a0a0a; letter-spacing: 0px;font-family:Montserrat;">  <b>Servicios</b> <br/></div>
    </li>
    <!-- SLIDE  -->
  </ul>
  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div>
</section>




<!--=================================
 service-->
 

<!--=================================
 Services Details -->

<!--=================================
 Services Details-->



<section class="white-bg page-section-ptb">
  <center>
  <div class="container">
          <div class="table-responsive">
           <div class="membership-pricing-table">
            <table>
                <tbody><tr>
                <th class="plan-header plan-header-blue">
                <div class="header-plan-inner">
                    <span class="recommended-plan-ribbon"><strike>$ 599 </strike>  </span>
                  <div class="pricing-plan-name">{{$servicios[1]->name}}</div>
                   <div class="pricing-plan-price">
                      <sup>$ {{$servicios[1]->amount}}</sup>

                  </div>
                </div>
                </th>
                <th class="plan-header plan-header-blue">
                <div class="header-plan-inner">
                    <span class="recommended-plan-ribbon"><strike>$ 599 </strike>  </span>
                  <div class="pricing-plan-name">{{$servicios[0]->name}}</div>
                   <div class="pricing-plan-price">
                      <sup>$ {{$servicios[0]->amount}}</sup>
                </div>
              </div>
                </th>
                <th class="plan-header plan-header-standard">
                <div class="header-plan-inner">
                    <!--<span class="plan-head"> </span>-->
                    <span class="recommended-plan-ribbon"><strike>$ 14.99 </strike>  </span>
                      <div class="pricing-plan-name">{{$servicios[3]->name}}</div>
                      <div class="pricing-plan-price">
                          <sup> $ {{$servicios[3]->amount}} (Free) </sup>
                    </div>
                   
                </div>
                </th>
                <th class="plan-header plan-header-blue">
                <div class="header-plan-inner">
                    <span class="recommended-plan-ribbon"><strike>$ 67.99 </strike>  </span>
                  <div class="pricing-plan-name">{{$servicios[2]->name}}</div>
                    <div class="pricing-plan-price">
                        <sup>$ {{$servicios[2]->amount}}</sup>
                    </div>
                </div>                  
                </th>
                </tr>
                <tr>

                    <td class="action-header">
                      <center>
                        <a class="button x-small" href="{{route('register')}}">
                            Comprar
                        </a>
                      </center>
                    </td>
                    <td class="action-header">
                        <a class="button x-small" href="{{route('register')}}">
                            Comprar
                        </a>
                    </td>
                    <td class="action-header">
                        <a class="button x-small" href="{{route('register')}}">
                            Comprar
                        </a>
                    </td>
                    <td class="action-header">
                        <a class="button x-small" href="{{route('register')}}">
                            Comprar
                        </a>
                    </td>
                </tr>
                <tr>
                    <td> <center>C4xAcademy advisories.</center></td>
                    <td> <center>Asesorías de C4xAcademy.</center></td>
                    <td> <center>Conóce nuestra Plataforma.</center></td>
                    <td> <center>Alertas Diarias del Mercado. </center></td>
                </tr>
                <tr>
                    <td> <center>C4xAcademy Audiovisual Classes. </center></td>
                    <td> <center>Clases AudioVisuales de C4xAcademy. </center></td>
                    <td> <center>Observa los videos gratuitos. </center></td>
                    <td> <center>Tips Certificados por C4xAcademy. </center></td>
                </tr>
                <tr>

                    <td> <center>C4xAcademy Complementary Files.</center></td>
                    <td> <center>Archivos Complementarios de C4xAcademy.</center></td>
                    <td> <center>Aprende con C4xAcademy. </center></td>
                    <td> <center>Fluctuaciones del Mercado. </center></td>
                </tr>
             
                
            </tbody></table>
              </div>
          </div>
        </div>

     </div>
</center>
</section>







<!--=================================
 Secciones -->
 
@include('layouts.footer') 
</div>
 
@include('layouts.scripts')

</body>
</html>