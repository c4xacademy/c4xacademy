<!DOCTYPE html>
<html lang="en">
@include('user_section.preview.layouts.htmlheader')
<body>

<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="{{URL::asset('page_assets/images/pre-loader/loader-02.svg')}}" alt="">
</div>

<!--=================================
 preloader -->

@include('user_section.preview.layouts.header')

<!--=================================
 Secciones -->

<section class="rev-slider">
  <div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-6" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
  <div id="rev_slider_16_1" class="fullwidthabanner" style="display:none;" data-version="5.4.5.2">
    <ul>  <!-- SLIDE  -->
    <li data-index="rs-33" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  data-delay="7990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
      <img src="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
      <div class="tp-caption   tp-resizeme"
        id="slide-34-layer-1"
        data-x="center" data-hoffset=""
        data-y="center" data-voffset="10"
        data-width="['auto']"
        data-height="['auto']"
        
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
      style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 100px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">Bienvenido a la <b>Sección de </b> <br/>Adquirir Servicios </div>
    </li>
    <!-- SLIDE  -->
  </ul>
</div>
</section>

<section class="white-bg page-section-ptb o-hidden">

      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif


  <div class="container">
    <div class="row"> 
       <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h6 class=""> </h6>
            <h2 class="title-effect">Comprar Servicio</h2>
            <p class=""> </p>
          </div>
      </div>
    </div>
    {!! Form::open(array('route' => 'usuario.servicios.store_adquirir','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div id="register-form" class="register-form">
           <div class="row">
 

                <div class="section-field">
                <label><b>Plataforma: </b></label>
                          <select onChange="pagoOnChangePlataforma(this)" id="plataforma" name="plataforma" class="form-control">
                                <option value="">Seleccionar Plataforma</option>
                                @if((Auth::User()->plataforma == 1) || (Auth::User()->plataforma == 2) || (Auth::User()->plataforma == 6) || (Auth::User()->plataforma == 3))
                                    <option value="0">C4xAcademy (Español)</option>
                                @endif
                                @if((Auth::User()->plataforma == 0) || (Auth::User()->plataforma == 2) || (Auth::User()->plataforma == 5) || (Auth::User()->plataforma == 3))
                                    <option value="1">C4xAcademy (Inglés/English)</option>
                                @endif
                                @if((Auth::User()->plataforma == 0) || (Auth::User()->plataforma == 1) || (Auth::User()->plataforma == 4) || (Auth::User()->plataforma == 3))
                                    <option value="2">Key Level Alerts</option>
                                @endif
                            @if ($errors->has('plataforma'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('plataforma') }}</strong>
                                </span>
                            @endif
                          </select>
               </div>

               <br>
                          <select onChange="pagoOnChange(this)" id="pago" name="pago" class="form-control">
                                <option value="">Seleccionar tipo de pago</option>
                                <option value="TRANSFERENCIA">Zelle</option>
                                <option value="BITCOINS">Bitcoin</option>
                                <option value="PAYPAL">PayPal</option>
                            @if ($errors->has('pago'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pago') }}</strong>
                                </span>
                            @endif
                          </select>


              <br>
            <div class="form-group" id="conf" style="display:none">
                              <input placeholder="Número de Confirmación" data-placement="top" data-toggle="tooltip" data-original-title="Si realiza el pago por transferencia, el número de confirmación es la referencia de la transferencia. Si el pago es por bitcoin, el número de confirmación es el wallet del que se realizó el pago." for="confirmacion" id="confirmacion" type="text" class="form-control" name="confirmacion" value="">
                                  @if ($errors->has('confirmacion'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('confirmacion') }}</strong>
                                      </span>
                                  @endif
              </div>



              <p><button class="button mt-20">Comprar</button></p>
         </div>
      </div>
    </div>
{!! Form::close() !!}
</section>



<!--=================================
 Secciones -->
 
@include('user_section.preview.layouts.footer')
</div>
 
@include('user_section.preview.perfil_usuario.layouts.scripts')
  <script type="text/javascript">
    function pagoOnChange(sel){
          if (sel.value=="PAYPAL"){
               divT = document.getElementById("conf");
               divT.style.display = "none";
          }
          if (sel.value=="BITCOINS"){
               divT = document.getElementById("conf");
               divT.style.display = "none";
               divT = document.getElementById("conf");
               divT.style.display = "";
          }
          if (sel.value=="TRANSFERENCIA"){
               divT = document.getElementById("conf");
               divT.style.display = "none";
               divT = document.getElementById("conf");
               divT.style.display = "";
          }
          if (sel.value==""){
               divT = document.getElementById("conf");
               divT.style.display = "none";
               divT = document.getElementById("conf");
               divT.style.display = "none";
          }
    }     


        function pagoOnChangePlataforma(sel){
              if (sel.value=="3"){ //C4xAcademy Preview
                   divT = document.getElementById("pago");
                   divT.style.display = "none";
                   divT = document.getElementById("conf");
                   divT.style.display = "none";
              }
              else{
                   divT = document.getElementById("pago");
                   divT.style.display = "";
              }
              if (sel.value=="2"){ //C4xAcademy Preview
                   divT = document.getElementById("pago");
                   divT.style.display = "none";
              }

        } 

  </script>

</body>
</html>