<!--=================================
 header -->

<header id="header" class="header fancy without-topbar text-dark">

<!--=================================
 mega menu -->
 
<div class="menu">  
  <div class="container"> 
    <div class="row"> 
     <div class="col-lg-12 col-md-12"> 
     <!-- menu start -->
       <nav id="menu" class="mega-menu">
        <!-- menu list items container -->
        <section class="menu-list-items">
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
                <a href="{{url('/')}}"><img id="logo_img" src="{{URL::asset('page_assets/images/logo.png')}}" alt="logo"> </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
         <li class="active"><a href="{{route('user_preview.index')}}">Inicio <i class="fa-indicator"></i></a></li>
            <li><a href="#">Videos <i class="fa fa-angle-down fa-indicator"></i></a>
                <!-- drop down multilevel  -->
                <ul class="drop-down-multilevel">
                  @foreach($categorias as $categoria)
                    <li><a href="{{route('user_preview.videos.categorias',['id' => Crypt::encrypt($categoria->categoria_id)])}}">{{$categoria->nombre}}<i class="ti-plus fa-indicator"></i> </a></li>
                  @endforeach
                </ul>
            </li>
         <li><a href="{{route('user_preview.perfil',['id' => Crypt::encrypt(Auth::user()->id)])}}">Perfil <i class="fa-indicator"></i></a></li>
         <li><a href="{{route('usuario.servicios_preview.adquirir')}}">Servicios <i class="fa-indicator"></i></a></li>
                 <!-- drop down multilevel  -->
        @if(Auth::check())
        <li><a href="{{route('login')}}">{{ Auth::user()->nombre }}<i class="fa fa-angle-down fa-indicator"></i></a>
        <div class="drop-down grid-col-12">
                                  <a href="{{ route('select') }}">Return</a>
                                  <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Cerrar Sesión
                                  </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                  </form>
        </div>
        @else
         <li><a href="{{route('login')}}">Login <i class="fa-indicator"></i></a></li>

        @endif
        </ul>
        </div>
       </section>
       </nav>
       </div>
     </div>
    </div>
   </div>
  <!-- menu end -->
</header>


<!--=================================
 header -->
