<!DOCTYPE html>
<html lang="en">
@include('user_section.preview.layouts.htmlheader')
<body>

<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="{{URL::asset('page_assets/images/pre-loader/loader-02.svg')}}" alt="">
</div>

<!--=================================
 preloader -->

@include('user_section.preview.layouts.header')

<!--=================================
 Secciones -->


<section class="rev-slider">
  <div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-6" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
  <div id="rev_slider_16_1" class="fullwidthabanner" style="display:none;" data-version="5.4.5.2">
    <ul>  <!-- SLIDE  -->
    <li data-index="rs-33" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  data-delay="7990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
      <img src="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
      <div class="tp-caption   tp-resizeme"
        id="slide-34-layer-1"
        data-x="center" data-hoffset=""
        data-y="center" data-voffset="10"
        data-width="['auto']"
        data-height="['auto']"
        
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
      style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 100px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">Bienvenido al Video <b>llamado: </b> <br/>{{$video->nombre}} </div>
    </li>
    <!-- SLIDE  -->
  </ul>
</div>
</section>

<!--=================================
 rev-slider -->

<!--=================================
about- -->

<!--=================================
about- -->

<!--=================================
Skill- -->






  <div class="col-lg-2 col-md-2">
  </div>
   <div class="col-lg-8 col-md-8">
       <div class="blog-entry mb-10">
            <div class="blog-entry-you-tube">
               <div class="js-video [youtube, widescreen]">
                {!!$video->iframe!!}
              </div>
          </div>
          <div class="blog-detail">
              <div class="entry-meta mb-10">
                  <p>{{$video->descripcion}}</p>
              </div>
          </div>
       </div>
    </div>
  <div class="col-lg-2 col-md-2">
  </div>



</div>
@include('user_section.preview.layouts.footer') 
@include('user_section.preview.videos.layouts.scripts')
<script type="text/javascript" language="javascript">
$(document).ready(function() {
            $(this).bind("contextmenu", function(e) {
                e.preventDefault();
             });
         });
</script>


</body>
</html>