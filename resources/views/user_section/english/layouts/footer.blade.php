<!--=================================
 footer -->
 
 <footer class="footer">

<div class="copyright gray-bg mt-50">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-sm-12">
          <center>
          <div>
             <p> C4xAcademy &copy; All Copyright is Reserved.<br>
            Developed with ♥ by: <a href="https://www.facebook.com/atomotecnologiasvirtuales/">Átomo Tecnologías Virtuales</a></p>
          </div>
          </center>
      </div>
    </div>
  </div>
</div>
</footer>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>Up</span></a></div>
<!--=================================
 footer -->