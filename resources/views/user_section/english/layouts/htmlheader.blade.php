<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="">
<meta name="author" content="Átomo Virtual">
<title>C4xAcademy</title>
<!-- Favicon -->
<link rel="shortcut icon" href="{{URL::asset('page_assets/images/favicon.ico')}}" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">
 
<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/plugins-css.css')}}" />


<!-- Typography -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/typography_users.css')}}" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/shortcodes/shortcodes.css')}}" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/style_users.css')}}" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/responsive.css')}}" /> 
 
@yield('css')

</head>