<!DOCTYPE html>
<html lang="en">
@include('user_section.key_level_alerts.layouts.htmlheader')
<body>

<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="{{URL::asset('page_assets/images/pre-loader/loader-02.svg')}}" alt="">
</div>

<!--=================================
 preloader -->

@include('user_section.key_level_alerts.layouts.header')

<!--=================================
 Secciones -->


<section class="rev-slider">
  <div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-6" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
  <div id="rev_slider_16_1" class="fullwidthabanner" style="display:none;" data-version="5.4.5.2">
    <ul>  <!-- SLIDE  -->
    <li data-index="rs-33" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  data-delay="7990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
      <img src="{{URL::asset('page_assets/revolution/assets/slider-06/95d68-bg.jpg')}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
      <div class="tp-caption   tp-resizeme"
        id="slide-34-layer-1"
        data-x="center" data-hoffset=""
        data-y="center" data-voffset="10"
        data-width="['auto']"
        data-height="['auto']"
        
        data-type="text"
        data-responsive_offset="on"
        data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
        data-textAlign="['center','center','center','center']"
        data-paddingtop="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingbottom="[0,0,0,0]"
        data-paddingleft="[0,0,0,0]"
      style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 100px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">Bienvenido a la <b>Sección de </b> <br/>Usuarios </div>
    </li>
    <!-- SLIDE  -->
  </ul>
</div>
</section>

<!--=================================
 rev-slider -->

<!--=================================
about- -->

<!--=================================
about- -->

<!--=================================
Skill- -->

<section class="o-hidden">
   <div class="container-fluid p-0">
      <div class="row row-eq-height no-gutter">
        <div class="col-lg-6 col-md-6 services-text-box-green">
            <div class="section-title">
                <h6 class="text-black">Que esperas para obtener la información actaulizada del mercado?</h6>
                <h2 class="text-black">¡Aprende, inspira y superate dentro de tu creencia en capacidad!</h2>
                <p class="text-black"></p>
             </div>
             <p class="text-black" align="justify">La Academia brinda la posibilidad de que se conozca el mercado de divisas apropiadamente desde una perpectiva diferente, fácil y atractiva al estudiante.</p>  
             <p class="text-black" align="justify"><span class="text-black" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tooltip on top">Te guiamos</span>  a convertirte en un trader experto en Forex enseñandote las habilidades y estrategias necesarias para tener éxito con el hecho de mantener las gráficas simples y accesibles para obtener un mejor entendimiento del análisis gráfico.</p>  
          </div>
        <div class="col-lg-6 col-md-6 services-text-box-black">

          </div>
    </div>
   </div>
</section>

<!--=================================
Skill- -->

<!--=================================
work-process -->


<!--=================================
work-process -->

<!--=================================
Meet our creative team  -->


<!--=================================
why-choose-us -->

<!--=================================
key-features -->


<!--=================================
key-features -->

<!--=================================
Our Clients  -->




<!--=================================
 Secciones -->
 
@include('user_section.key_level_alerts.layouts.footer')
</div>
 
@include('user_section.key_level_alerts.layouts.scripts')

</body>
</html>