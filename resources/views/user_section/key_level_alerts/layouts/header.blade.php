<!--=================================
 header -->

<header id="header" class="header fancy without-topbar text-dark">

<!--=================================
 mega menu -->
 
<div class="menu">  
  <div class="container"> 
    <div class="row"> 
     <div class="col-lg-12 col-md-12"> 
     <!-- menu start -->
       <nav id="menu" class="mega-menu">
        <!-- menu list items container -->
        <section class="menu-list-items">
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
                <a href="{{url('/')}}"><img id="logo_img" src="{{URL::asset('page_assets/images/logo.png')}}" alt="logo"> </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
         <li class="active"><a href="{{route('key_level_alerts.index')}}">Inicio <i class="fa-indicator"></i></a></li>
         <li><a href="{{route('key_level_alerts.perfil',['id' => Crypt::encrypt(Auth::user()->id)])}}">Perfil <i class="fa-indicator"></i></a></li>
                 <!-- drop down multilevel  -->
         <li><a href="{{route('usuario.servicios_key.adquirir')}}">Servicios <i class="fa-indicator"></i></a></li>

        @if(Auth::check())
        <li><a href="{{route('login')}}">{{ Auth::user()->nombre }}<i class="fa fa-angle-down fa-indicator"></i></a>
        <div class="drop-down grid-col-12">
                                  <a href="{{ route('select') }}">Regresar</a>
                                  <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Cerrar Sesión
                                  </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                  </form>
        </div>
        @else
         <li><a href="{{route('login')}}">Login <i class="fa-indicator"></i></a></li>

        @endif
        </ul>
        </div>
       </section>
       </nav>
       </div>
     </div>
    </div>
   </div>
  <!-- menu end -->
</header>


<!--=================================
 header -->
