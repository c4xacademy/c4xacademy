<!--=================================
 jquery -->

<!-- jquery -->
<script type="text/javascript" src="{{asset('page_assets/js/jquery-1.12.4.min.js')}}"></script>

<!-- plugins-jquery -->
<script type="text/javascript" src="{{asset('page_assets/js/plugins-jquery.js')}}"></script>

<!-- plugin_path -->
<script type="text/javascript">var plugin_path = 'page_assets/js/';</script>
 

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{asset('page_assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('page_assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<!-- revolution custom --> 
<script type="text/javascript" src="{{asset('page_assets/revolution/js/revolution-custom.js')}}"></script> 

<!-- custom -->
<script type="text/javascript" src="{{asset('page_assets/js/custom.js')}}"></script>

@yield('js')