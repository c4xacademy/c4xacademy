@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Servicios</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.servicios.create')}}" class="btn_1 medium">Agregar Servicio</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Servicios </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Servicio</th>
                  <th>Precio</th>
                  <th>Acción</th
                </tr>
              </thead>
              <tbody>
                @foreach($servicios as $servicio)
                <tr>
                  <td><center>{{$servicio->name}}</center></td>
                  <td><center>{{$servicio->amount}}</center></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.servicios.edit', $parameters = ['id' => Crypt::encrypt($servicio->id)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.servicios.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($servicio->id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Servicio?')"]);!!}
                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection