@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Editar Servicios</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Servicios</h2>
      </div>
    {!! Form::open(array('route' => 'administrador.servicios.update','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Nombre: </label>
              <input id="name" type="text" class="form-control" name="name" placeholder="Nombre del Servicio" value="{{$servicio->name}}">
                @if ($errors->has('name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Precio: </label>
              <input id="amount" type="text" class="form-control" name="amount" placeholder="Descripción del Archivo" value="{{$servicio->amount}}">
                @if ($errors->has('amount'))
                  <span class="help-block">
                    <strong>{{ $errors->first('amount') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
    </div>
      <!-- /row-->
 
    </div>
    <!-- /box_general-->
    <input type="hidden" name="id" value="{{$servicio->id}}">
    <p><button class="btn_1 medium">Modificar</button></p>
    </div>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection