@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Promocodes</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.promocodes.create')}}" class="btn_1 medium">Agregar Promocode</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Promocodes </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Estado</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($promocodes as $promocode)
                <tr>
                  <td><center>{{$promocode->code}}</center></td>
                  <td><center>
                                  @if($promocode->habilitado == 0)
									Inactivo
                                  @endif
                                  @if($promocode->habilitado == 1)
									Activo
                                  @endif
                  </center></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.promocodes.edit', $parameters = ['id' => Crypt::encrypt($promocode->id)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.promocodes.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($promocode->id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Promocode?')"]);!!}
 
                                     @if($promocode->habilitado == '0' || $promocode->habilitado == null)
                                        {!!link_to_route('administrador.EstadoPromocode', $title = 'Habilitar',
                                        $parameters = ['id' => Crypt::encrypt($promocode->id)],
                                        $attributes = ['class'=>'btn btn-success']);!!}
                                    @endif

                                    @if($promocode->habilitado == '1')
                                    {!!link_to_route('administrador.EstadoPromocode', $title = 'Deshabilitar',
                                    $parameters = ['id' => Crypt::encrypt($promocode->id)],
                                    $attributes = ['class'=>'btn btn-danger']);!!}
                                    @endif

                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection