@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Editar Promocodes</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Promocodes</h2>
      </div>
    {!! Form::open(array('route' => 'administrador.promocodes.update','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Nombre: </label>
              <input id="code" type="text" class="form-control" name="code" placeholder="Promocode" value="{{$promocode->code}}">
                @if ($errors->has('code'))
                  <span class="help-block">
                    <strong>{{ $errors->first('code') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
      <!-- /row-->
 
    </div>
    <!-- /box_general-->
    <input type="hidden" name="id" value="{{$promocode->id}}">
    <p><button class="btn_1 medium">Modificar</button></p>
    </div>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection