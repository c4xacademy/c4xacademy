@extends('admin_section.layouts.app')

@section('main-content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Usuarios</li>
      </ol>
    <p><a href = "{{route('administrador.usuario.create')}}" class="btn_1 medium">Agregar Usuario</a></p>

      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Usuarios </div>
        <div class="card-body">
          <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombres</th>
                  <th>Correo</th>
                  <th>Teléfono</th>
                  <th>Promocode</th>
                  <th>Plataforma</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td><center>{{$user->nombre}}</center></td>
                  <td><center>{{$user->email}}</center></td>
                  <td><center>
                    @if($user->telefono === null)
                      No Aplica
                    @else
                      {{$user->telefono}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->promocode === null)
                      No Aplica
                    @else
                      {{$user->promocode}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->plataforma == 0)
                      C4xAcademy Español
                    @endif
                    @if($user->plataforma == 1)
                      C4xAcademy Inglés/English
                    @endif
                    @if($user->plataforma == 2)
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 3)
                      C4xAcademy Preview
                    @endif
                    @if($user->plataforma == 4)
                      C4xAcademy Español<br>
                      C4xAcademy Inglés/English
                    @endif
                    @if($user->plataforma == 5)
                      C4xAcademy Español<br>
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 6)
                      C4xAcademy Inglés/English<br>
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 7)
                      C4xAcademy Español<br>
                      C4xAcademy Inglés/English<br>
                      Key Level Alerts
                    @endif
                  </center></td>
                  <td><center>
                                <a class="btn btn-success" href="{{route('administrador.usuario.edit', $parameters = ['id' => Crypt::encrypt($user->id)], $absolute = true)}}">Modificar</a><br>
                                {!!link_to_route('administrador.listadousuarios.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($user->id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Usuario?')"]);!!}                    
                  </center></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection