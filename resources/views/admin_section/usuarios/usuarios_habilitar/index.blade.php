@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Usuarios por Habilitar</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Usuarios por Habilitar </div>
        <div class="card-body">
          <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombres</th>
                  <th>Correo</th>
                  <th>Número de Confirmación</th>
                  <th>Teléfono</th>
                  <th>Promocode</th>
                  <th>Comprobante Paypal</th>
                  <th>Plataforma</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td><center>{{$user->nombre}}</center></td>
                  <td><center>{{$user->email}}</center></td>
                  <td><center>
                    @if($user->confirmacion == null)
                      No Aplica
                    @else
                    {{$user->confirmacion}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->telefono == null)
                      No Aplica
                    @else
                    {{$user->telefono}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->promocode == null)
                      No Aplica
                    @else
                    {{$user->promocode}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->comprobante == null)
                      No Aplica
                    @else
                    <font color=blue><a style="color:#0000ff" href="{{URL::to($user->comprobante)}}" target="_blank"><center>Ver Comprobante</center></a></font>
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->plataforma == 0)
                      C4xAcademy Español
                    @endif
                    @if($user->plataforma == 1)
                      C4xAcademy Inglés/English
                    @endif
                    @if($user->plataforma == 2)
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 4)
                      C4xAcademy Español<br>
                      C4xAcademy Inglés/English
                    @endif
                    @if($user->plataforma == 5)
                      C4xAcademy Español<br>
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 6)
                      C4xAcademy Inglés/English<br>
                      Key Level Alerts
                    @endif
                    @if($user->plataforma == 7)
                      C4xAcademy Español<br>
                      C4xAcademy Inglés/English<br>
                      Key Level Alerts
                    @endif
                  </center></td>
                  <td><center>
                                {!!link_to_route('administrador.habilitarusuario', $title = 'Habilitar',
                                           $parameters = Crypt::encrypt($user->id),
                                           $attributes = ['class'=>'btn btn-success']);!!}
                                @if($user->bandera_plataforma !== null)
                                {!!link_to_route('administrador.deshabilitarusuario', $title = 'No-Habilitar',
                                           $parameters = Crypt::encrypt($user->id),
                                           $attributes = ['class'=>'btn btn-warning']);!!}
                                @endif

                                {!!link_to_route('administrador.listadousuarios.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($user->id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Usuario?')"]);!!}                    
                  </center></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection