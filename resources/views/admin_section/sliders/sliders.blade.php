@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Sliders</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.sliders.create')}}" class="btn_1 medium">Agregar Slider</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Sliders </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Slider</th>
                  <th>Descripción</th>
                  <th>Descripción 2</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sliders as $slider)
                <tr>
                  <td><center><img src="{{URL::asset($slider->sliders_ruta_imagen)}}" width="40%" height="10%"/></center></td>
                  <td><center>{{$slider->sliders_descripcion}}</center></td>
                  <td><center>{{$slider->sliders_descripcion_2}}</center></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.sliders.edit', $parameters = ['id' => Crypt::encrypt($slider->sliders_cod)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.sliders.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($slider->sliders_cod)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Slider?')"]);!!}
                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection