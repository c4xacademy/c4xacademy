@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Crear Sliders</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Sliders</h2>
      </div>
    {!! Form::open(array('route' => 'administrador.sliders.store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Descripción</label>
              <input id="descripcion" type="text" class="form-control" name="descripcion" placeholder="Descripción">
                @if ($errors->has('descripcion'))
                  <span class="help-block">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Descripción 2</label>
              <input id="descripcion_2" type="text" class="form-control" name="descripcion_2" placeholder="Descripción 2">
                @if ($errors->has('descripcion_2'))
                  <span class="help-block">
                    <strong>{{ $errors->first('descripcion_2') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
      <!-- /row-->
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Imagen del Slider</label>
              <input id="sliders_ruta_imagen" type="file" class="form-control" name="sliders_ruta_imagen" placeholder="Imagen">
                @if ($errors->has('sliders_ruta_imagen'))
                  <span class="help-block">
                    <strong>{{ $errors->first('sliders_ruta_imagen') }}</strong>
                  </span>
                @endif
          </div>
        </div>
 
      </div>
      <!-- /row-->
 
    </div>
    <!-- /box_general-->

    <p><button class="btn_1 medium">Crear</button></p>
    </div>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection