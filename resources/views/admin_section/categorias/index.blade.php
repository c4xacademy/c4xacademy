@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Categorías</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.categorias.create')}}" class="btn_1 medium">Agregar Categoría</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Categorías </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Plataforma</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($categorias as $categoria)
                <tr>
                  <td><center>{{$categoria->nombre}}</center></td>
                  <td><center>
                                  @if($categoria->plataforma == 0)
                                    C4xAcademy (Español)
                                  @endif
                                  @if($categoria->plataforma == 1)
                                    C4xAcademy (Inglés/English)
                                  @endif
                                  @if($categoria->plataforma == 3)
                                    C4xAcademy (Preview)
                                  @endif
                  </center></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.categorias.edit', $parameters = ['id' => Crypt::encrypt($categoria->categoria_id)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.categorias.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($categoria->categoria_id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar esta Categoría? Si la elimina se borrarán los videos asociados a dicha categoría.')"]);!!}
 
                                     @if($categoria->habilitada == '0' || $categoria->habilitada == null)
                                        {!!link_to_route('administrador.EstadoCategoria', $title = 'Habilitar',
                                        $parameters = ['id' => Crypt::encrypt($categoria->categoria_id)],
                                        $attributes = ['class'=>'btn btn-success']);!!}
                                    @endif

                                    @if($categoria->habilitada == '1')
                                    {!!link_to_route('administrador.EstadoCategoria', $title = 'Deshabilitar',
                                    $parameters = ['id' => Crypt::encrypt($categoria->categoria_id)],
                                    $attributes = ['class'=>'btn btn-danger']);!!}
                                    @endif

                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection