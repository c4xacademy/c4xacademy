@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Crear Categoría</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    {!! Form::open(array('route' => 'administrador.categorias.store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Categorías</h2>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Nombre</label>
              <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre de la Categoría">
                @if ($errors->has('nombre'))
                  <span class="help-block">
                    <strong>{{ $errors->first('nombre') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Plataforma: </label>
                          <select id="plataforma" name="plataforma" class="form-control">
                                <option value="">Seleccionar Plataforma</option>
                                <option value="0">C4xAcademy (Español)</option>
                                <option value="1">C4xAcademy (Inglés/English)</option>
                                <option value="3">C4xAcademy (Preview)</option
                            @if ($errors->has('plataforma'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('plataforma') }}</strong>
                                </span>
                            @endif
                          </select>
          </div>
        </div>
      </div>
      <!-- /row--> 
    </div>
    <!-- /box_general-->
    </div>
        <p><button class="btn_1 medium">Crear</button></p>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection