<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Átomo Virtual">
  <title>C4xAcademy - Panel de Administración</title>
    
  <!-- Favicons-->
  <link rel="shortcut icon" href="{{URL::asset('admin_section/img/favicon.ico')}}" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="{{URL::asset('admin_section/img/apple-touch-icon-57x57-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{URL::asset('admin_section/img/apple-touch-icon-72x72-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{URL::asset('admin_section/img/img/apple-touch-icon-114x114-precomposed.png')}}">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{URL::asset('admin_section/img/img/apple-touch-icon-114x114-precomposed.png')}}">

  <!-- Bootstrap core CSS-->
  <link href="{{asset('admin_section/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <link href="{{ asset('admin_section/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <!-- Main styles -->
  <link href="{{asset('admin_section/css/admin.css')}}" rel="stylesheet">
  <!-- Icon fonts-->
  <link href="{{asset('admin_section/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Plugin styles -->
  <link href="{{asset('admin_section/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <link href="{{asset('admin_section/vendor/dropzone.css')}}" rel="stylesheet">
  <!-- Your custom styles -->
  <link href="{{asset('admin_section/css/custom.css')}}" rel="stylesheet">

  <style type="text/css">
    div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 {
        min-height: .01%;
        overflow-x: auto;
    }
    @media screen and (max-width: 767px) {
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            border: 1px solid #ddd;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table {
            margin-bottom: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > thead > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tbody > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tfoot > tr > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > thead > tr > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tbody > tr > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table > tfoot > tr > td {
            white-space: nowrap;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered {
            border: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > th:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > td:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > td:first-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > td:first-child {
            border-left: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > th:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > thead > tr > td:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr > td:last-child,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr > td:last-child {
            border-right: 0;
        }
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr:last-child > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr:last-child > th,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tbody > tr:last-child > td,
        div.table-responsive-dt div.dataTables_wrapper div.row:nth-child(2) > div.col-sm-12 > .table-bordered > tfoot > tr:last-child > td {
            border-bottom: 0;
        }
        /* FIN DEL HACK JQUERY-DATATABLES */
    }
    
  </style>

  @yield('css')
</head>
