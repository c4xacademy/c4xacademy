    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>
C4xAcademy &copy; Se reserva Todos los Derechos de Autor.
            Desarrollado con ♥ por: <a href="https://www.facebook.com/atomotecnologiasvirtuales/">Átomo Tecnologías Virtuales
          </small>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>