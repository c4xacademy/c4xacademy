      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{route('administrador')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Principal</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{route('administrador.perfil')}}">
            <i class="fa fa-fw fa-plus-circle"></i>
            <span class="nav-link-text">Perfil</span>
          </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{route('administrador.sliders')}}">
            <i class="fa fa-fw fa-archive"></i>
            <span class="nav-link-text">Sliders</span>
          </a>
    </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My profile">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseProfile" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Usuarios</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseProfile">
            <li>
              <a href="{{route('administrador.listadousuarios')}}">Listado Usuarios</a>
            </li>
            <li>
              <a href="{{route('administrador.listadousuarioshabilitar')}}">Habilitar Usuarios</a>
            </li>
          </ul>
        </li>

    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-gear"></i>
            <span class="nav-link-text">Videos</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="{{route('administrador.categorias')}}">Categorías</a>
            </li>
              <li>
              <a href="{{route('administrador.videos')}}">Listado Videos</a>
            </li>
          </ul>
    </li>

    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{route('administrador.archivos')}}">
            <i class="fa fa-fw fa-plus-circle"></i>
            <span class="nav-link-text">Archivos</span>
          </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{route('administrador.servicios')}}">
            <i class="fa fa-fw fa-plus-circle"></i>
            <span class="nav-link-text">Servicios</span>
          </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="fdsfsd">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#sdgskjklf" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-gear"></i>
            <span class="nav-link-text">Key Level Alerts</span>
          </a>
          <ul class="sidenav-second-level collapse" id="sdgskjklf">
            <li>
              <a href="{{route('administrador.listadousuarioskey')}}">Listado Usuarios</a>
            </li>
              <li>
              <a href="#">Enviar Mensaje</a>
            </li>
          </ul>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{route('administrador.promocodes')}}">
            <i class="fa fa-fw fa-plus-circle"></i>
            <span class="nav-link-text">Promocodes</span>
          </a>
    </li>


      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>