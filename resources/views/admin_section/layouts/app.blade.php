<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@include('admin_section.layouts.htmlheader')

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-default fixed-top" id="mainNav">
		@include('admin_section.layouts.boton')
    <div class="collapse navbar-collapse" id="navbarResponsive">
		@include('admin_section.layouts.header')
		@include('admin_section.layouts.sidebar')
    </div>
  </nav>
  <!-- /Navigation-->
  <!--Contenido de la página-->
    @yield('main-content')
  <!--/Contenido de la página-->

    <!-- /.container-wrapper-->

@include('admin_section.layouts.footer')
@include('admin_section.layouts.scripts')
@include('admin_section.layouts.logout_modal')	
</body>
</html>