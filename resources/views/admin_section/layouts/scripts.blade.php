  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('admin_section/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('admin_section/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{asset('admin_section/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <!-- Page level plugin JavaScript-->

  <script src="{{asset('admin_section/vendor/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('admin_section/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('admin_section/vendor/jquery.selectbox-0.2.js')}}"></script>
  <script src="{{asset('admin_section/vendor/retina-replace.min.js')}}"></script>
  <script src="{{asset('admin_section/vendor/jquery.magnific-popup.min.js')}}"></script>
  <!-- Custom scripts for all pages-->
  <script src="{{asset('admin_section/js/admin.js')}}"></script>

  <!-- Custom scripts for this page-->

  <script type="text/javascript">
  $(function () {
    $('#dataTable').DataTable({
          "language": {
            "lengthMenu": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info": "Ver _PAGE_ de _PAGES_",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    }); 
  });   
  </script>


@yield('js')