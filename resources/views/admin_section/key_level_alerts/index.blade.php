@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Usuarios Key Level Alerts</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Usuarios Key Level Alerts</div>
        <div class="card-body">
          <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombres</th>
                  <th>Correo</th>
                  <th>Teléfono</th>
                  <th>Cuenta Telegram</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td><center>{{$user->nombre}}</center></td>
                  <td><center>{{$user->email}}</center></td>
                  <td><center>
                    @if($user->telefono === null)
                      No Aplica
                    @else
                      {{$user->telefono}}
                    @endif
                  </center></td>
                  <td><center>
                    @if($user->telegram === null)
                      No Aplica
                    @else
                      {{$user->telegram}}
                    @endif
                  </center></td>
                  <td><center>
                                <a class="btn btn-success" href="{{route('administrador.usuario.edit', $parameters = ['id' => Crypt::encrypt($user->id)], $absolute = true)}}">Modificar</a><br>
                                {!!link_to_route('administrador.listadousuarios.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($user->id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Usuario?')"]);!!}                    
                  </center></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection