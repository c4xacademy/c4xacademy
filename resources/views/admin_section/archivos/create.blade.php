@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Crear Archivo</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Archivos</h2>
      </div>
    {!! Form::open(array('route' => 'administrador.archivos.store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Nombre: </label>
              <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre del Archivo">
                @if ($errors->has('nombre'))
                  <span class="help-block">
                    <strong>{{ $errors->first('nombre') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Descripción: </label>
              <input id="descripcion" type="text" class="form-control" name="descripcion" placeholder="Descripción del Archivo">
                @if ($errors->has('descripcion'))
                  <span class="help-block">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
      <!-- /row-->
       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Archivo: </label>
              <input id="ruta_archivo" type="file" class="form-control" name="ruta_archivo" placeholder="Archivo">
                @if ($errors->has('ruta_archivo'))
                  <span class="help-block">
                    <strong>{{ $errors->first('ruta_archivo') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
    </div>
    <!-- /box_general-->

    <p><button class="btn_1 medium">Crear</button></p>
    </div>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection