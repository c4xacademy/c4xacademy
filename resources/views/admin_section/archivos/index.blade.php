@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Archivos</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.archivos.create')}}" class="btn_1 medium">Agregar Archivo</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Archivos </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Archivo</th>
                  <th>Acción</th
                </tr>
              </thead>
              <tbody>
                @foreach($archivos as $archivo)
                <tr>
                  <td><center>{{$archivo->nombre}}</center></td>
                  <td><center>{{$archivo->descripcion}}</center></td>
                  <td><font color=blue><a style="color:#0000ff" href="{{URL::to($archivo->ruta_archivo)}}" target="_blank"><center>Ver Archivo</center></a></font></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.archivos.edit', $parameters = ['id' => Crypt::encrypt($archivo->archivo_id)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.archivos.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($archivo->archivo_id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Archivo?')"]);!!}
                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection