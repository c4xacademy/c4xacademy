@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Videos</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Example DataTables Card-->
    <p><a href = "{{route('administrador.videos.create')}}" class="btn_1 medium">Agregar Video</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Videos </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Posición</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Plataforma</th>
                  <th>Categoría</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($videos as $video)
                <tr>
                  <td><center>{{$video->posicion}}</center></td>
                  <td><center>{{$video->nombre}}</center></td>
                  <td><center>{{$video->descripcion}}</center></td>
                  <td><center>
                    @if($video->plataforma == 0)
                      C4xAcademy (Español)
                    @endif
                    @if($video->plataforma == 1)
                      C4xAcademy (Inglés/English)
                    @endif
                  </center></td>
                  <td><center>{{$video->nombre_categoria}}</center></td>
                  <td><center><a class="btn btn-success" href="{{route('administrador.videos.edit', $parameters = ['id' => Crypt::encrypt($video->video_id)], $absolute = true)}}">Modificar</a>
                                {!!link_to_route('administrador.videos.delete', $title = 'Eliminar',
                                    $parameters = ['id' => Crypt::encrypt($video->video_id)],
                                    $attributes = ['class'=>'btn btn-danger', 'onclick'=>"return confirm('¿Seguro que desea Eliminar este Video?')"]);!!}
                  </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <!-- /tables-->
    </div>
    <!-- /container-fluid-->
    </div>



@endsection