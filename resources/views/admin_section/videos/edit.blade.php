@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Editar Videos</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <div class="box_general padding_bottom">
      <div class="header_box version_2">
        <h2><i class="fa fa-file"></i>Videos</h2>
      </div>
    {!! Form::open(array('route' => 'administrador.videos.update','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Nombre: </label>
              <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$video->nombre}}">
                @if ($errors->has('nombre'))
                  <span class="help-block">
                    <strong>{{ $errors->first('nombre') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Descripción: </label>
              <input id="descripcion" type="text" class="form-control" name="descripcion" placeholder="Descripción" value="{{$video->descripcion}}">
                @if ($errors->has('descripcion'))
                  <span class="help-block">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                  </span>
                @endif
          </div>
        </div>
      </div>
      <!-- /row-->

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Iframe Youtube: </label>
              <input id="iframe" type="text" class="form-control" name="iframe" placeholder="Iframe Youtube" value="{{$video->iframe}}">
                @if ($errors->has('iframe'))
                  <span class="help-block">
                    <strong>{{ $errors->first('iframe') }}</strong>
                  </span>
                @endif
          </div>
        </div>
        <div class="col-md-6">
              <div class="form-group">
                <label>Categoría: </label>
                            {!! Form::select('categoria_id', $categorias, $categoria->categoria_id, array('class' => 'form-control', 'placeholder' => 'Seleccionar Categoría')) !!}
              </div>
        </div>
       </div>
      <!-- /row-->
 
       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Plataforma: </label>
                          <select id="plataforma" name="plataforma" class="form-control">
                                <option value="{{$video->plataforma}}">
                                  @if($video->plataforma == 0)
                                    C4xAcademy (Español)
                                  @endif
                                  @if($video->plataforma == 1)
                                    C4xAcademy (Inglés/English)
                                  @endif
                                  @if($video->plataforma == 3)
                                    C4xAcademy (Preview)
                                  @endif
                                </option>
                                <option value="">Seleccionar Plataforma</option>
                                <option value="0">C4xAcademy (Español)</option>
                                <option value="1">C4xAcademy (Inglés/English)</option>
                                <option value="3">C4xAcademy (Preview)</option>
                            @if ($errors->has('plataforma'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('plataforma') }}</strong>
                                </span>
                            @endif
                          </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Posición: </label>
              <input id="posicion" type="number" class="form-control" name="posicion" placeholder="Posición del Video" value="{{$video->posicion}}"> 
                @if ($errors->has('posicion'))
                  <span class="help-block">
                    <strong>{{ $errors->first('posicion') }}</strong>
                  </span>
                @endif
          </div>
      </div>

    </div>
    <!-- /box_general-->
    <input type="hidden" name="id" value="{{$video->video_id}}">
    <p><button class="btn_1 medium">Modificar</button></p>
    </div>
    <!-- /.container-fluid-->
    </div>
{!! Form::close() !!}


@endsection