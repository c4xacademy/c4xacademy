@extends('admin_section.layouts.app')

@section('main-content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('administrador')}}">Panel de Administración</a>
        </li>
        <li class="breadcrumb-item active">Principal</li>
      </ol>
      @if (count($errors) > 0)
        <strong></strong><br>
        <div class="alert alert-danger">
            <ul>
              <p>{{ $errors }}</p>
            </ul>
        </div>
      @endif

      @if ($message = Session::get('success'))
          <strong></strong><br>
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
    <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-envelope-open"></i>
              </div>
              <div class="mr-5"><h5>{{$cuenta_usuarios_habilitados->usuarios_habilitados}} Usuarios Habilitados!</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('administrador.listadousuarios')}}">
              <span class="float-left">Ver Usuarios</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-star"></i>
              </div>
        <div class="mr-5"><h5>{{$cuenta_usuarios_no_habilitados->usuarios_no_habilitados}} Usuarios Por Habilitar!</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('administrador.listadousuarioshabilitar')}}">
              <span class="float-left">Ver Usuarios</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-calendar-check-o"></i>
              </div>
              <div class="mr-5"><h5>{{$cuenta_videos->videos_total}} Videos en la Plataforma!</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('administrador.videos')}}">
              <span class="float-left">Ver Videos</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-heart"></i>
              </div>
              <div class="mr-5"><h5>{{$categorias->categorias_cuenta}} Categorías de Videos en la Plataforma!</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('administrador.categorias')}}">
              <span class="float-left">Ver Categorías</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
    </div>
    <!-- /cards -->
    <h2></h2>
    </div>
    <!-- /.container-fluid-->
    </div>



@endsection