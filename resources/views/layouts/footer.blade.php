<!--=================================
 footer -->
 
 <footer class="footer">

<div class="copyright black-bg mt-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-sm-12">
          <center>
          <div>
             <p> C4xAcademy &copy; Se reserva Todos los Derechos de Autor.<br>
            Desarrollado con ♥ por: <a href="https://www.facebook.com/atomotecnologiasvirtuales/">Átomo Tecnologías Virtuales</a></p>
          </div>
          </center>
      </div>
    </div>
  </div>
</div>
</footer>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>Subir</span></a></div>
<!--=================================
 footer -->