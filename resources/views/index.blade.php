<!DOCTYPE html>
<html lang="en">
@include('layouts.htmlheader')
<!-- Style -->
<link rel="stylesheet" type="text/css" href="{{asset('page_assets/css/style.css')}}" />

<body>

<div class="wrapper">

<!--=================================
 preloader -->
 
<div id="pre-loader">
    <img src="{{URL::asset('page_assets/images/pre-loader/loader-02.svg')}}" alt="">
</div>

<!--=================================
 preloader -->

@include('layouts.header')

<!--=================================
 Secciones -->


<section class="rev-slider">
  <div id="rev_slider_16_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-6" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
  <div id="rev_slider_16_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.2">
    <ul>  <!-- SLIDE  -->
    @foreach($sliders as $slide)
    <li data-index="rs-33" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{URL::asset($slide->sliders_ruta_imagen)}}"  data-delay="5990"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
      <img src="{{URL::asset($slide->sliders_ruta_imagen)}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
      <!-- LAYER NR. 4 -->
    </li>
    @endforeach
    <!-- SLIDE  -->
  </ul>
  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div>
</section>

<!--=================================
 rev-slider -->

<!--=================================
about- -->

<section class="about page-section-ptb">
  <div class="container">
    <div class="row">
       <div class="col-lg-8 col-md-8 col-md-offset-2">
       <div class="section-title text-center">
            <h2 class="title-effect">Nuestros Servicios </h2>
          </div>
      </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="feature-text round theme-icon text-center xs-mb-30">
            <div class="feature-icon">
              <span aria-hidden="true" class="ti-layers-alt theme-color"></span>
            </div>
              <div class="feature-info">
              <h4 class="pb-10"><a href="{{route('register')}}"> C4xAcademy</a></h4>
              <p align="justify">Hogar de las estrategias más poderosas que encontrarás en Forex. Aprende los fundamentos para involucrarte en una industria versátil y llena de oportunidades.</p>
          </div>
         </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="feature-text round theme-icon text-center xs-mb-30">
            <div class="feature-icon">
              <span aria-hidden="true" class="ti-shield theme-color"></span>
            </div>
              <div class="feature-info">
              <h4 class="pb-10"><a href="{{route('register')}}"> C4xAcademy Inglés/English</a></h4>
              <p align="justify">Expandiendo el hogar de las estrategias más poderosas que encontrarás en Forex al idioma inglés, encontrarás el mismo contenido que en nuestro curso en español pero dirigido a nuestro público de habla inglesa.</p>
          </div>
         </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="feature-text round theme-icon text-center">
            <div class="feature-icon">
              <span aria-hidden="true" class="ti-bell theme-color"></span>
            </div>
              <div class="feature-info">
              <h4 class="pb-10"><a href="{{route('register')}}"> Key Level Alerts</a></h4>
              <p align="justify">No deje escapar ninguna oportunidad gracias a "Key Level Alerts", donde al subscribirse recibirá periódicamente alertas de las variaciones del mercado.</p>
          </div>
         </div> 
        </div>
     </div>
  </div>
</section>

<!--=================================
about- -->

<!--=================================
Skill- -->

<section class="o-hidden">
   <div class="container-fluid p-0">
      <div class="row row-eq-height no-gutter">
  <div class="col-lg-6 col-md-4 services-text-box-green">
      <div class="section-title">
          <h6 class="text-black">Somos C4xAcademy</h6>
          <p class="text-black">Aprende y gana con personas en las que puedes confiar.</p>
       </div>
       <p class="text-black" align="justify">Somos su socio, su asesor de negocios y su maestro, todo en uno. Creemos en la transparencia al 100%, le mostramos los buenos tiempos y los no tan buenos tiempos. Nuestro objetivo es ayudarte en ser rentable exitosamente  y ser 100 % transparente durante el transcurso a ello!</p>  
       <p class="text-black" align="justify">
          La Academia como objetivo tiene el deber de dar a conocer el mercado de divisas apropiadamente desde una perspectiva diferente fácil y atractiva al estudiante, Apasionadamente con el saber de ayudar a otros en su situación financiera con el conocimiento brindado.
       </p>
       <p class="text-black" align="justify">
          Le mostramos cómo convertirse en un trader experto en Forex enseñándole las habilidades y estrategias necesarias para tener éxito con el hecho de mantener las gráficas simples y sencillas para obtener un mejor entendimiento del análisis gráfico.
       </p>
    </div>
  <div class="col-lg-3 col-md-4 services-text-box-black">
    <div class=" text-white">
      <div class="section-title">
          <h6 class="text-white"></h6>
          <h2 class="text-white title-effect">Habilidades</h2>
       </div>
       <div class="skill-bar">
            <div class="progress-bar" data-percent="100" data-delay="0" data-type="%">
                <div class="progress-title">Forex</div>
            </div>
         </div>
         <div class="skill-bar">
            <div class="progress-bar" data-percent="100" data-delay="0" data-type="%">
                <div class="progress-title">Trading</div>
            </div>
         </div>
         <div class="skill-bar">
            <div class="progress-bar" data-percent="100" data-delay="0" data-type="%">
                <div class="progress-title">Análisis</div>
            </div>
         </div>
         <div class="skill-bar">
            <div class="progress-bar" data-percent="100" data-delay="0" data-type="%">
                <div class="progress-title">Inversión</div>
            </div>
         </div>
        </div>
       </div>
      <div class="col-lg-3 col-md-4 hidden-md-down" data-jarallax='{"speed": 1}'  style="background: url('{{asset('page_assets/images/IMG-2532.JPG')}}') no-repeat 0 0;">
      </div>
    </div>
   </div>
</section>

<!--=================================
Skill- -->

<!--=================================
work-process -->

<section class="page-section-ptb">
 <div class="container">
  <div class="row">
    <div class="col-lg-4 col-md-4">
       <div class="section-title mb-20">
          <h6>Nuestras Estadísticas </h6>
          <h2>C4xAcademy hace las cosas diferentes.</h2>
                 <p class="text-white" align="justify">El poder observar las vidas impactadas de manera positiva no tiene descripción al igual del poder dar la oportunidad de tener una vida económicamente abundante con los conocimientos dados y aplicados apropiadamente de la academia</p>
        </div> 
    </div>
    <div class="col-lg-7 col-md-7 col-md-offset-1 sm-mt-40">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">           
            <div class="feature-text left-icon mb-40">
              <div class="feature-icon">
              <span class="ti-user theme-color" aria-hidden="true"></span>
              </div>
              <div class="feature-info">
                <center>
                  <div class="counter text-white">
                      <span class="icon "></span>
                      <span class="timer" data-to="{{$users}}" data-speed="2000">{{$users}}</span>
                      <label>Usuarios Inscritos en la Plataforma</label>
                  </div>
                </center>
             </div>
           </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">           
            <div class="feature-text left-icon mb-40">
              <div class="feature-icon">
              <span class="ti-desktop theme-color" aria-hidden="true"></span>
              </div>
              <div class="feature-info">
                <center>
                  <div class="counter text-white">
                      <span class="icon "></span>
                      <span class="timer" data-to="{{$videos_espaniol}}" data-speed="2000">{{$videos_espaniol}}</span>
                      <label>Videos en la Plataforma en Español</label>
                  </div>
                </center>
             </div>
           </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">           
            <div class="feature-text left-icon mb-40">
              <div class="feature-icon">
              <span class="ti-desktop theme-color" aria-hidden="true"></span>
              </div>
              <div class="feature-info">
                <center>
                  <div class="counter text-white">
                      <span class="icon "></span>
                      <span class="timer" data-to="{{$videos_ingles}}" data-speed="2000">{{$videos_ingles}}</span>
                      <label>Videos en la Plataforma en Inglés</label>
                  </div>
                </center>
             </div>
           </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">           
            <div class="feature-text left-icon">
              <div class="feature-icon">
              <span class="ti-user theme-color" aria-hidden="true"></span>
              </div>
              <div class="feature-info">
                <center>
                  <div class="counter text-white">
                      <span class="icon "></span>
                      <span class="timer" data-to="{{$users_key}}" data-speed="2000">{{$users_key}}</span>
                      <label>Usuarios con "Key Level Alerts"</label>
                  </div>
                </center>
             </div>
           </div>
          </div>
        </div>
    </div>
  </div>
 </div>
</section>

<!--=================================
work-process -->

<!--=================================
Meet our creative team  -->

<section class="page-section-ptb gray-bg">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">  
            <h2 class="title-effect">Nuestro Creador</h2>
          </div>
       </div>
    </div>
      <div class="row">
        <div class="col-lg-4 col-md-4 sm-mb-30"> 
        </div>       
        <div class="col-lg-6 col-md-4 sm-mb-30">
           <div class="team team-list">
              <div class="team-photo">
                <img class="img-responsive center-block" src="{{URL::asset('page_assets/images/creador.jpeg')}}" alt=""> 
              </div>    
              <div class="team-description"> 
                <div class="team-info"> 
                     <h5><a href="team-single.html">Mauricio Ceballos</a></h5>
                     <span>CEO</span>
                </div>
                <div class="team-contact">
                  <span class="email"> <i class="fa fa-envelope-o"></i> c4xacademy.web@gmail.com</span>
                </div>
                <div class="social-icons color clearfix">
                    <ul>
                      <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                      <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                 </div>
               </div>
           </div>
          </div>
        <div class="col-lg-4 col-md-4 sm-mb-30"> 
        </div>       
       </div>
  </div>
</section>


 
@include('layouts.footer') 
</div>
 
@include('layouts.scripts')

</body>
</html>