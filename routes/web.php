<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/sms/send/{to}', function(\Nexmo\Client $nexmo, $to){
    $message = $nexmo->message()->send([
        'to' => $to,
        'from' => '@leggetter',
        'text' => 'Prueba Real por la noche!'
    ]);
    Log::info('sent message: ' . $message['message-id']);
});



//Stripe
Route::post('/stripe/pago', ['as' => 'pago.stripe', 'uses' => 'SuscripcionController@pago']);

Route::get('stripe/suscripcion', ['as' => 'stripe.suscripcion', 'uses' => 'SuscripcionController@vista_suscripcion']);

Route::post('/stripe/procesa-suscripcion', ['as' => 'suscripcion.stripe', 'uses' => 'SuscripcionController@procesa_suscripcion']);

Route::get('stripe/suscripcion/interna', ['as' => 'stripe.suscripcion_interna', 'uses' => 'SuscripcionInternaController@vista_suscripcion_interna']);

Route::post('/stripe/procesa-suscripcion/interna', ['as' => 'suscripcion_interna.stripe', 'uses' => 'SuscripcionInternaController@procesa_suscripcion']);



//Stripe


////Rutas Paypal/////


Route::get('/paypal/{order?}','PayPalController@form')->name('order.paypal');
Route::get('/checkout/payment/{order}/paypal','PayPalController@checkout')->name('checkout.payment.paypal');
Route::get('/paypal/checkout/{order}/completed','PayPalController@completed')->name('paypal.checkout.completed');
Route::get('/paypal/checkout/{order}/cancelled','PayPalController@cancelled')->name('paypal.checkout.cancelled');
Route::post('/webhook/paypal/{order?}/{env?}','PayPalController@webhook')->name('webhook.paypal.ipn');
Route::get('payment-completed/{order}','PayPalController@paymentCompleted')->name('paymentCompleted');

///interno////
Route::get('/paypal/interno/{order?}','PayPalInternoController@form')->name('order.paypal');
Route::get('/checkout/interno/payment/{order}/paypal','PayPalInternoController@checkout')->name('checkout.payment.paypal.interno');
Route::get('/paypal/checkout/interno/{order}/completed','PayPalInternoController@completed')->name('paypal.checkout.completed.interno');
Route::get('/paypal/checkout/interno/{order}/cancelled','PayPalInternoController@cancelled')->name('paypal.checkout.cancelled.interno');
Route::post('/webhook/paypal/interno/{order?}/{env?}','PayPalInternoController@webhook')->name('webhook.paypal.ipn.interno');
Route::get('payment-completed/interno/{order}','PayPalInternoController@paymentCompleted_interno')->name('paymentCompleted_interno');
///////////////
////Rutas Paypal/////


Route::post('/select_view', ['as' => 'auth.select', 'uses' => 'HomeController@select_store'])->middleware('auth', 'role:USUARIO');

//Rutas Administrador//

	//Rutas Sliders

Route::get('administrador/sliders/delete/{id}', ['as' => 'administrador.sliders.delete', 'uses' => 'SliderController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/sliders/edit', ['as' => 'administrador.sliders.update', 'uses' => 'SliderController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/sliders/edit/{id}', ['as' => 'administrador.sliders.edit', 'uses' => 'SliderController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/sliders/create', ['as' => 'administrador.sliders.store', 'uses' => 'SliderController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/sliders/create', ['as' => 'administrador.sliders.create', 'uses' => 'SliderController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/sliders', ['as' => 'administrador.sliders', 'uses' => 'SliderController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/sliders', ['as' => 'administrador_update.sliders', 'uses' => 'SliderController@modificar_slider'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Sliders//


	//Rutas Categorías

Route::get('administrador/categorias/delete/{id}', ['as' => 'administrador.categorias.delete', 'uses' => 'CategoriaController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/categorias/edit', ['as' => 'administrador.categorias.update', 'uses' => 'CategoriaController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/categorias/edit/{id}', ['as' => 'administrador.categorias.edit', 'uses' => 'CategoriaController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/categorias/create', ['as' => 'administrador.categorias.store', 'uses' => 'CategoriaController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/categorias/create', ['as' => 'administrador.categorias.create', 'uses' => 'CategoriaController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/categorias', ['as' => 'administrador.categorias', 'uses' => 'CategoriaController@index'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('administrador/categorias/{id}', ['as' => 'administrador.EstadoCategoria', 'uses'      => 'CategoriaController@EstadoCategoria'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Categorías//



	//Rutas Promocodes

Route::get('administrador/promocodes/delete/{id}', ['as' => 'administrador.promocodes.delete', 'uses' => 'PromocodeController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/promocodes/edit', ['as' => 'administrador.promocodes.update', 'uses' => 'PromocodeController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/promocodes/edit/{id}', ['as' => 'administrador.promocodes.edit', 'uses' => 'PromocodeController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/promocodes/create', ['as' => 'administrador.promocodes.store', 'uses' => 'PromocodeController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/promocodes/create', ['as' => 'administrador.promocodes.create', 'uses' => 'PromocodeController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/promocodes', ['as' => 'administrador.promocodes', 'uses' => 'PromocodeController@index'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('administrador/promocodes/{id}', ['as' => 'administrador.EstadoPromocode', 'uses'      => 'PromocodeController@EstadoPromocode'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Promocodes//



	//Rutas Archivos

Route::get('administrador/archivos/delete/{id}', ['as' => 'administrador.archivos.delete', 'uses' => 'ArchivoController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/archivos/edit', ['as' => 'administrador.archivos.update', 'uses' => 'ArchivoController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/archivos/edit/{id}', ['as' => 'administrador.archivos.edit', 'uses' => 'ArchivoController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/archivos/create', ['as' => 'administrador.archivos.store', 'uses' => 'ArchivoController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/archivos/create', ['as' => 'administrador.archivos.create', 'uses' => 'ArchivoController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/archivos', ['as' => 'administrador.archivos', 'uses' => 'ArchivoController@index'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Archivos//



	//Rutas Servicios

Route::get('administrador/servicios/delete/{id}', ['as' => 'administrador.servicios.delete', 'uses' => 'ServiceController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/servicios/edit', ['as' => 'administrador.servicios.update', 'uses' => 'ServiceController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/servicios/edit/{id}', ['as' => 'administrador.servicios.edit', 'uses' => 'ServiceController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/servicios/create', ['as' => 'administrador.servicios.store', 'uses' => 'ServiceController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/servicios/create', ['as' => 'administrador.servicios.create', 'uses' => 'ServiceController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/servicios', ['as' => 'administrador.servicios', 'uses' => 'ServiceController@index'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Servicios//




	//Rutas Perfil Administrador
Route::get('administrador/perfil', ['as' => 'administrador.perfil', 'uses' => 'AdministradorController@perfil'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/perfil', ['as' => 'administrador_update.perfil', 'uses' => 'AdministradorController@update_perfil'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador', ['as' => 'administrador', 'uses' => 'AdministradorController@index'])->middleware('auth', 'role:ADMINISTRADOR');
	//Fin Rutas Perfil Administrador


	//Rutas Usuarios Administrador
Route::get('administrador/usuario/delete/{id}', ['as' => 'administrador.listadousuarios.delete', 'uses' => 'AdministradorController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/usuario/create', ['as' => 'administrador.usuario.store', 'uses' => 'AdministradorController@store_administrador'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/usuario/edit', ['as' => 'administrador.usuario.update', 'uses' => 'AdministradorController@update_administrador'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/usuario/create', ['as' => 'administrador.usuario.create', 'uses' => 'AdministradorController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/usuario/edit/{id}', ['as' => 'administrador.usuario.edit', 'uses' => 'AdministradorController@edit'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/listadousuarios', ['as' => 'administrador.listadousuarios', 'uses' => 'AdministradorController@listadoUsuarios'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/listadousuarioskey', ['as' => 'administrador.listadousuarioskey', 'uses' => 'AdministradorController@listadoUsuarioskey'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/listadousuarioshabilitar', ['as' => 'administrador.listadousuarioshabilitar', 'uses' => 'AdministradorController@listadousuarioshabilitar'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/habilitarusuario/{id}', ['as' => 'administrador.habilitarusuario', 'uses' => 'AdministradorController@habilitarUsuarioAdministrador'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/deshabilitarusuario/{id}', ['as' => 'administrador.deshabilitarusuario', 'uses' => 'AdministradorController@deshabilitarUsuarioAdministrador'])->middleware('auth', 'role:ADMINISTRADOR');


	//Fin Rutas Usuarios Administrador


	//Rutas Videos

Route::get('administrador/videos/delete/{id}', ['as' => 'administrador.videos.delete', 'uses' => 'VideoController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('administrador/videos/edit', ['as' => 'administrador.videos.update', 'uses' => 'VideoController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/videos/edit/{id}', ['as' => 'administrador.videos.edit', 'uses' => 'VideoController@edit'])->middleware('auth', 'role:ADMINISTRADOR');

Route::post('administrador/videos/create', ['as' => 'administrador.videos.store', 'uses' => 'VideoController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/videos/create', ['as' => 'administrador.videos.create', 'uses' => 'VideoController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('administrador/videos', ['as' => 'administrador.videos', 'uses' => 'VideoController@index'])->middleware('auth', 'role:ADMINISTRADOR');

	//Fin Rutas Videos//




//Fin Rutas Administrador

Route::get('/', ['as' => 'inicial', 'uses' => 'PageController@inicial']);
Route::get('/servicios', ['as' => 'servicios', 'uses' => 'PageController@servicios']);
Route::get('/select', ['as' => 'select', 'uses' => 'HomeController@index'])->middleware('auth', 'role:USUARIO');





//Inicio Rutas Usuarios Key Level Alerts//

Route::get('key_level_alerts/', ['as' => 'key_level_alerts.index', 'uses' => 'UserKeyLevelAlertsController@index'])->middleware('auth', 'role:USUARIO');
Route::get('key_level_alerts/perfil/{id}', ['as' => 'key_level_alerts.perfil', 'uses' => 'UserKeyLevelAlertsController@perfil'])->middleware('auth', 'role:USUARIO');
Route::post('key_level_alerts/perfil/edit', ['as' => 'key_level_alerts.perfil.update', 'uses' => 'UserKeyLevelAlertsController@update_perfil'])->middleware('auth', 'role:USUARIO');

//Fin Rutas Usuarios Key Level Alerts//



//Inicio Rutas Usuarios Español//

Route::get('usuario/', ['as' => 'usuario.index', 'uses' => 'UserController@index_espaniol'])->middleware('auth', 'role:USUARIO');
Route::get('usuario/perfil/{id}', ['as' => 'usuario.perfil', 'uses' => 'UserController@perfil'])->middleware('auth', 'role:USUARIO');
Route::post('usuario/perfil/edit', ['as' => 'usuario.perfil.update', 'uses' => 'UserController@update_perfil'])->middleware('auth', 'role:USUARIO');

Route::get('usuario/archivos', ['as' => 'usuario.archivos', 'uses' => 'UserController@archivos_academia'])->middleware('auth', 'role:USUARIO');
Route::get('usuario/videos/categorias/{id}', ['as' => 'usuario.videos.categorias', 'uses' => 'VideoController@listado_videos_categoria'])->middleware('auth', 'role:USUARIO');
Route::get('usuario/videos/reproduccion/{id}', ['as' => 'usuario.videos.reproduccion', 'uses' => 'VideoController@video_reproduccion'])->middleware('auth', 'role:USUARIO');

//Fin Rutas Usuarios Español//


//Inicio Rutas Usuarios Inglés//

Route::get('user/', ['as' => 'user.index', 'uses' => 'UserEnglishController@index_english'])->middleware('auth', 'role:USUARIO');
Route::get('user/profile/{id}', ['as' => 'user.perfil', 'uses' => 'UserEnglishController@perfil_english'])->middleware('auth', 'role:USUARIO');
Route::post('user/profile/edit', ['as' => 'user.perfil.update', 'uses' => 'UserEnglishController@update_perfil'])->middleware('auth', 'role:USUARIO');

Route::get('user/files', ['as' => 'user.archivos', 'uses' => 'UserEnglishController@archivos_academia'])->middleware('auth', 'role:USUARIO');
Route::get('user/videos_english/categorias/{id}', ['as' => 'user.videos.categorias', 'uses' => 'VideoController@listado_videos_categoria_english'])->middleware('auth', 'role:USUARIO');
Route::get('user/videos/reproduccion_english/{id}', ['as' => 'user.videos.reproduccion', 'uses' => 'VideoController@video_reproduccion_english'])->middleware('auth', 'role:USUARIO');

//Fin Rutas Usuarios Inglés//



//Inicio Rutas Usuarios Preview//

Route::get('user_preview/', ['as' => 'user_preview.index', 'uses' => 'UserPreviewController@index_preview'])->middleware('auth', 'role:USUARIO');
Route::get('user_preview/profile/{id}', ['as' => 'user_preview.perfil', 'uses' => 'UserPreviewController@perfil_preview'])->middleware('auth', 'role:USUARIO');
Route::post('user_preview/profile/edit', ['as' => 'user_preview.perfil.update', 'uses' => 'UserPreviewController@update_perfil'])->middleware('auth', 'role:USUARIO');

Route::get('user_preview/videos_preview/categorias/{id}', ['as' => 'user_preview.videos.categorias', 'uses' => 'VideoController@listado_videos_categoria_preview'])->middleware('auth', 'role:USUARIO');
Route::get('user_preview/videos/reproduccion_preview/{id}', ['as' => 'user_preview.videos.reproduccion', 'uses' => 'VideoController@video_reproduccion_preview'])->middleware('auth', 'role:USUARIO');

//Fin Rutas Usuarios Preview//


///// Perfil /////

Route::get('usuario/servicios/adquirir', ['as' => 'usuario.servicios.adquirir', 'uses' => 'ServiceController@adquirir'])->middleware('auth', 'role:USUARIO');
Route::get('usuario/servicios_preview/adquirir', ['as' => 'usuario.servicios_preview.adquirir', 'uses' => 'ServiceController@adquirir_preview'])->middleware('auth', 'role:USUARIO');
Route::get('usuario/servicios_key/adquirir', ['as' => 'usuario.servicios_key.adquirir', 'uses' => 'ServiceController@adquirir_key'])->middleware('auth', 'role:USUARIO');
Route::get('user/services/buy', ['as' => 'user.services.buy', 'uses' => 'ServiceController@buy'])->middleware('auth', 'role:USUARIO');
Route::post('usuario/servicios/adquirir', ['as' => 'usuario.servicios.store_adquirir', 'uses' => 'ServiceController@store_adquirir'])->middleware('auth', 'role:USUARIO');


//////////////////








